<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Error</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{ asset('backoffice/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('backoffice/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('backoffice/css/core.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('backoffice/css/components.css')}}" rel="stylesheet" type="text/css">
	<link href="{{ asset('backoffice/css/colors.css')}}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{{ asset('backoffice/js/plugins/loaders/pace.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('backoffice/js/core/libraries/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('backoffice/js/core/libraries/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{ asset('backoffice/js/plugins/loaders/blockui.min.js')}}"></script>
	<!-- /core JS files -->


	<!-- Theme JS files -->
	<script type="text/javascript" src="{{ asset('backoffice/js/core/app.js')}}"></script>
	<!-- /theme JS files -->

</head>

<body class="login-container">

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Content area -->
				<div class="content">

					<!-- Error title -->
					<div class="text-center content-group">
						<h1 class="error-title">404</h1>
						<h5>Oops, an error has occurred. Page not found!</h5>
					</div>
					<!-- /error title -->


					<!-- Error content -->
					<div class="row">
						<div class="col-lg-4 col-lg-offset-4 col-sm-6 col-sm-offset-3">
							<form action="#" class="main-search">
								<div class="row">
									<div class="col-sm-12">
										<a href="{{url('')}}" class="btn btn-primary btn-block content-group"><i class="icon-circle-left2 position-left"></i> Go Back</a>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- /error wrapper -->

				</div>
				<!-- /content area -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->

</body>

<!-- Mirrored from demo.interface.club/limitless/layout_1/LTR/default/error_404.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 22 Nov 2016 10:47:32 GMT -->
</html>
