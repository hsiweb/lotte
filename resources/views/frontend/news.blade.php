<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title> Lotte | Home </title>
	@include('frontend.includes.styles')
</head>
<body>
	
    <div class="fakeloader"></div>
	@include('frontend.includes.header')

	<div class="banner" style="margin-top: 80px;">
		<div class="container-fluid">
			<div class="col-sm-10 col-sm-offset-1 text-center txt-white">
				<h1 class="mv">NEWS</h1>
			</div>

		</div>
	</div>
	<div class="container marg-bot" >


		<div class="col-sm-10 col-sm-offset-1">
			

			@if($count==0)
			<div class="col-sm-12 text-center" style="margin-bottom: 50px;padding: 10% 0;">
				<h1 class="moment" style="color: #000;">Page still not available</h1>
			</div>
			@else

			<!-- -------- PAG MY LAMAN NA ------- -->
			<div class="col-sm-12">
				@foreach($news as $index=>$info)
				<div class="row single-news">
					<img src="{{$info->directory.'/'.$info->filename}}" alt="" class="img-responsive pull-left" style="width: 35%; padding: 20px;">
				    
				      <h3 class="txt-red">{{$info->title}}</h3>
				      <p class="text-justify">{!!html_entity_decode(Str::limit($info->content,$limit="250",$end="..."),ENT_QUOTES, 'UTF-8')!!}
				      </p>
				      <h4 class="text-right link" style="margin-top: 100px;"><a href="news/{{$info->id}}">CONTINUE READING</a></h4>
				</div>
				@endforeach
			</div>
			@endif
		</div>
	</div>



	@include('frontend.includes.footer')
</body>
</html>