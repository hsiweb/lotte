<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title> Lotte | Careers </title>
	@include('frontend.includes.styles')
</head>
<body>
	
    <div class="fakeloader"></div>
	@include('frontend.includes.header')

	<div class="banner" style=" margin-top:80px;">
		<div class="container-fluid">
			<div class="col-sm-10 col-sm-offset-1 text-center txt-white">
				<h1 class="career-head">WANT TO JOIN OUR TEAM?</h1>
			</div>
		</div>
	</div>

	<div class="aboutContent">
		@foreach($careers as $index=>$info)
		<img src="{{asset($info->directory.'/'.$info->filename)}}" alt="" width="100%">
		@endforeach
	</div>
	<div class="banner" style="background-color:#dd2f3c!important; padding: 15px 0 ;">
		<h2 class="txt-white text-center bs" style="font-weight: 400;">IF YOU THINK YOU'VE GOT WHAT IT TAKES, <br>
		HERE ARE THE CAREER OPPORTUNITIES FOR YOU!</h2>
	</div>
	<div class="container">
		<div class="col-sm-10 col-sm-offset-1" style="padding:20px 0;color: #000;">
			<div class="col-sm-12" style="margin-bottom: 50px;">
			@foreach(array_chunk($career,2) as $index=>$info)
				<div class="row">
				@foreach($info as $index => $row)
					<div class="col-sm-6 history2line">
						<div class="c-carrer text-justify">
							<div>
								<h2 style="margin-bottom: 10px!important;" class="text-left">{{$row['position']}}</h2>
								{!!html_entity_decode($row['position_description'],ENT_QUOTES, 'UTF-8')!!}
								<br>
								<div class="row">
									<p class="col-sm-6"><a href="{{route('frontend.career_details',[$row['id']])}}" style="background: #fff; border-radius: 5px; padding: 5px 10px;">See Details</a></p>
									<p class="career text-right" style="margin-right: 20px;"><a href="mailto:info@lottepilipinas.com" style="background: #fff; border-radius: 5px; padding: 5px 10px;">Apply</a></p>
								</div>
							</div>
						</div>
					</div>
				@endforeach
				</div>
			@endforeach
			</div>
		</div>
	</div>

	@include('frontend.includes.footer')
</body>
</html>