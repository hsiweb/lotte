<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title> Lotte | About </title>
	@include('frontend.includes.styles')
	<link href="{{asset('frontend/plugins/jquery.rotate/css/style.css?v=1.3')}}" rel="stylesheet" />
</head>
<body>

    <div class="fakeloader"></div>
	
	@include('frontend.includes.header')
	<div class="about">
            <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1default" data-toggle="tab">HISTORY</a></li>
                            <li><a href="#tab2default" data-toggle="tab">MISSION VISION</a></li>
                            <li><a href="#tab3default" data-toggle="tab">CORE VALUES</a></li>
                        </ul>
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1default">
                        		@foreach($about_history as $index=>$info)
								<img src="{{asset($info->directory.'/'.$info->filename)}}" alt="" width="100%">
								@endforeach
					        <div class="container">
								<div class="col-sm-10 col-sm-offset-1 " style="padding: 35px 0 10px 0">
									<p class="txt-red text-justify">Lotte was founded in 1948. The company name of Lotte comes from the nickname of Charlotte, the heroine in the novel, “The Sorrows of Young Werther”, written by the great German literary figure Johann Wolfgang von Goethe. This name was used to represent the desire of the company to be loved by everyone.</p>
								</div>
                        	</div>
                        	<div class="">
								<div id="slideImg">
									<div class="slideMain">
									    <ul class="slide1">
									      @foreach($image_slider as $index=>$info)
									      <li><img src="{{asset($info->directory.'/'.$info->filename.'?v=1.1')}}"></li>
									      @endforeach
									    </ul>
									</div>
								</div>
                        	</div>
                        	<div class="container">
                        		<div class="col-sm-10 col-sm-offset-1 text-justify">
                        			<p>Today, Lotte has grown into a general sweets producer familiar as “Everyday Sweet Life” through the wide variety of confectionery products we offer including chewing gum, candies, chocolate, biscuits, and ice cream. <br><br>

                        			In addition, the company has diversified into a wide range of fields in Japan and Korea as represented by Lotte Hotel, Lotte World (theme park), Lotte Mart (supermarket), Lotte Department Stores, Lotte Duty Free Stores, the Lotterria (a hamburger & fastfood chain), the Chiba Lotte Marines (a professional baseball team), and health industry products. Lotte has continuously grown into a global company group with overseas expansion – mainly in Asia and Europe. <br><br>

                        			The Lotte Group will continue expanding its activities to remain a company loved by everyone. <br><br>

                        			Our goal is to become a company that is appreciated by more and more consumers every day and to produce products that are loved by as many people as possible.</p>


									<div class="pres-corner">
										<p class="text-center"><img src="{{ asset('frontend/img/avatar3.png?v=1.2')}}" style="margin: 25px auto;width: 100%;" 	alt=""></p>
										<p>In Lotte Confectionery Pilipinas Corporation, we work with a purpose – and that is to bring sweet & delightful experience to every Filipino. We are proud to have been fulfilling this purpose since 2009.
										<br><br>
										We started by selling chewing gums in the early years. In our pursuit to reach more Filipinos, we have extended our product offerings in a matter of two years. From being a popular gum manufacturer, Lotte is now known in the Philippines as a credible maker of premium confectionery products such as Koala’s March, Choco Pie, and Pepero. In fact, Pepero is now the strong leader in the imported stick biscuit segment in the Philippines. Without a doubt, we, in Lotte, are succeeding in bringing enjoyable moments to every Filipino.
										<br><br>
										Looking ahead, we are eager to introduce more Lotte brands. We commit to strive harder in producing high-quality & high-value brands that will continue to bring sweet & delightful experience to Filipinos nationwide.</p>
										<br>
										<br>
										<p class="text-right">
											– Hiroshi Matsubara <br>
											President, Lotte Confectionery Pilipinas Corporation

										</p>
									</div>
                        		</div>
                        	</div>
		            	</div>	


                        <div class="tab-pane fade" id="tab2default">
								
							@foreach($about_mission_vision as $index=>$info)
							<img src="{{asset($info->directory.'/'.$info->filename)}}" alt="" width="100%">
							@endforeach
					        <div class="container-fluid">
								<div class="col-sm-10 col-sm-offset-1">
									<div class="col-sm-12 row">
										<div class="col-sm-6 text-center" style="padding: 0 6%;">
											<h1 class="txt-red mv" style="margin-bottom: 20px;">MISSION</h1>
											@foreach($tagline as $index=>$info)
											@if($info->type=="mission")
											<p class="mont-light">{!!html_entity_decode($info->content,ENT_QUOTES, 'UTF-8')!!}</p>
											@endif
											@endforeach
										</div>
										<div class="col-sm-6 text-center" style="padding: 0 6%;">
											<h1 class="txt-red mv" style="margin-bottom: 20px;">VISION</h1>
											@foreach($tagline as $index=>$info)
											@if($info->type=="vision")
											<p class="mont-light">{!!html_entity_decode($info->content,ENT_QUOTES, 'UTF-8')!!}</p>
											@endif
											@endforeach
										</div>
									</div>

								</div>
                        	</div>
                        </div>


                        <div class="tab-pane fade" id="tab3default">
								@foreach($about_core_values as $index=>$info)
								<img src="{{asset($info->directory.'/'.$info->filename)}}" alt="" width="100%">
								@endforeach
							<div class="banner">
					        	<div class="container-fluid">
									<div class="col-sm-10 col-sm-offset-1 text-center txt-white">
										<p class="text-justify">The success of the company lies in the values that are shared by its people. These set of core values reflect the company’s principles and are based on the belief that passionate & innovative people provide the best products and services to our customers for their enjoyment.</p>
									</div>

								</div>
                        	</div>
                        	<div class="container-fluid">
									<div class="col-sm-10 col-sm-offset-1 text-left core">
										<h1 class="m-r text-center" style="margin-top: 40px;letter-spacing: 10px;font-size: 50px;">LOTTE TREATS</h1>
										<h2 class="txt-red">TRUST</h2>
										@foreach($tagline as $index=>$info)
										@if($info->type=="trust")
										<p>{!!html_entity_decode($info->content,ENT_QUOTES, 'UTF-8')!!}</p>
										@endif
										@endforeach

										<h2 class="txt-red">RESPECT</h2>
										@foreach($tagline as $index=>$info)
										@if($info->type=="respect")
										<p>{!!html_entity_decode($info->content,ENT_QUOTES, 'UTF-8')!!}</p>
										@endif
										@endforeach

										<h2 class="txt-red">ENTHUSIASM</h2>
										@foreach($tagline as $index=>$info)
										@if($info->type=="enthusiasm")
										<p>{!!html_entity_decode($info->content,ENT_QUOTES, 'UTF-8')!!}</p>
										@endif
										@endforeach

										<h2 class="txt-red">ACCOUNTABILITY</h2>
										@foreach($tagline as $index=>$info)
										@if($info->type=="accountability")
										<p>{!!html_entity_decode($info->content,ENT_QUOTES, 'UTF-8')!!}</p>
										@endif
										@endforeach

										<h2 class="txt-red">TEAM EXCELLENCE</h2>
										@foreach($tagline as $index=>$info)
										@if($info->type=="excellence")
										<p>{!!html_entity_decode($info->content,ENT_QUOTES, 'UTF-8')!!}</p>
										@endif
										@endforeach
										
										<h2 class="txt-red">SPIRIT OF CHALLENGE</h2>
										@foreach($tagline as $index=>$info)
										@if($info->type=="challenge")
										<p>{!!html_entity_decode($info->content,ENT_QUOTES, 'UTF-8')!!}</p>
										@endif
										@endforeach

									</div>

								</div>
                        </div>
                    
		        </div>
			</div>
    	</div>
	</div>

	@include('frontend.includes.footer')
	
	  

	<script src="{{asset('frontend/js/jquery.bxslider.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('frontend/plugins/jquery.rotate/js/jquery.rotate.js?v=1.2')}}"></script>
	<script type="text/javascript">
		$(function(){
			$("#slideImg").rotate();
		});
	</script>
</body>
</html>