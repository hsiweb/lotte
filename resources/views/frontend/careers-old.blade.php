<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title> Lotte | Careers </title>
	@include('frontend.includes.styles')
</head>
<body>
	
    <div class="fakeloader"></div>
	@include('frontend.includes.header')

	<div class="banner" style=" margin-top:80px;">
		<div class="container-fluid">
			<div class="col-sm-10 col-sm-offset-1 text-center txt-white">
				<h1 class="career-head">WANT TO JOIN OUR TEAM?</h1>
			</div>
		</div>
	</div>

	<div class="aboutContent">
		@foreach($careers as $index=>$info)
		<img src="{{asset($info->directory.'/'.$info->filename)}}" alt="" width="100%">
		@endforeach
	</div>
	<div class="banner" style="background-color:#dd2f3c!important; padding: 15px 0 ;">
		<h2 class="txt-white text-center bs" style="font-weight: 400;">IF YOU THINK YOU'VE GOT WHAT IT TAKES, <br>
		HERE ARE THE CAREER OPPORTUNITIES FOR YOU!</h2>
	</div>
	<div class="container">
		<div class="col-sm-10 col-sm-offset-1" style="padding:20px 0;color: #000;">
			<div class="col-sm-12" style="margin-bottom: 50px;">
			@foreach(array_chunk($career,2) as $index=>$info)
				<div class="row">
				@foreach($info as $index => $row)
					<div class="col-sm-6 history2line">
					    <h2 style="margin-bottom: 0!important; margin-top: 50px!important;">{{$row['position']}}</h2>
					    {!!html_entity_decode($row['position_description'],ENT_QUOTES, 'UTF-8')!!}
					    <br>
					    <p class="career"><a href="mailto:info@lottepilipinas.com">APPLY (click)</a></p>
					</div>
				@endforeach
				</div>
			@endforeach
			</div>
		</div>
	</div>

	@include('frontend.includes.footer')
</body>
</html>