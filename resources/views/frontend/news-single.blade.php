<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title> Lotte | Home </title>
	@include('frontend.includes.styles')
</head>
<body>
	
    <div class="fakeloader"></div>
	@include('frontend.includes.header')

	<div class="banner" style="margin-top: 80px;">
		<div class="container-fluid">
			<div class="col-sm-10 col-sm-offset-1 text-center txt-white">
				<h1 class="mv">NEWS & PROMOS</h1>
			</div>
		</div>
	</div>
	<div class="container single-news" style="padding-bottom:50px; border-bottom: none">
		  <div class="col-sm-10 col-sm-offset-1 text-justify">
		  	@if($count==0)
			<div class="col-sm-12 text-center" style="margin-bottom: 50px;padding: 10% 0;">
				<h1 class="moment" style="color: #000;">News not found</h1>
				<h4 class="text-center link"><a href="{{route('frontend.news')}}" class="btn btn-default"><span class="fa fa-arrow-circle-o-left"></span> Go Back</a></h4>
			</div>
			@else
		  	@foreach($news as $index=>$info)
		  	<p class="txt-red text-center h1" style="font-size: 45px;text-transform: uppercase; margin-top: 40px;">{{$info->title}}</p>
			  <p style="padding-top:20px;">
			  	 <img src="{{ asset($info->directory.'/'.$info->filename)}}" alt="" width="50%" style="margin: 0 20px 20px 20px; float: left">
			  	{!!html_entity_decode($info->content,ENT_QUOTES, 'UTF-8')!!}
			  </p>
			@endforeach
			@endif
		  </div>
		</div>
		<div class="banner news-banner" style="padding: 10px 0; margin: 20px 0;">
			<p class="text-center" style="font-size: 25px;color: #fff; text-transform: uppercase;">Other News</p>
		</div>
		<div class="container">
			
			<div class="row">
			@foreach($newss as $index=>$info)
				<div class="col-sm-4">
					<div class="news">
						<p>{{$info->title}}</p>
						<a href="news/{{$info->id}}"><img src="{{ asset($info->directory.'/'.$info->filename)}}" alt="" width="100%"></a>
					</div>
				</div>
			@endforeach
			</div>
		</div>


	@include('frontend.includes.footer')
</body>
</html>