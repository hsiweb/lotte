<script src="{{ asset('frontend/js/jquery-2.2.3.min.js')}}"></script>
<!-- fullPage slider -->
<script type="text/javascript" src="{{ asset('frontend/js/jquery.fullPage.min.js')}}"></script>
<!-- Bootstrap JavaScript -->
<script src="{{ asset('frontend/js/bootstrap.min.js')}}"></script>
<!-- isotop -->
<script type="text/javascript" src="{{ asset('frontend/js/isotope.pkgd.min.js')}}"></script>
<!-- swiper -->
<script type="text/javascript" src="{{ asset('frontend/js/idangerous.swiper.min.js')}}"></script>
<!-- popup -->
<script src="{{ asset('frontend/js/index.js')}}"></script>
<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-75073666-4', 'auto');
	ga('send', 'pageview');
</script>