@if(Session::get('notification-status')=="success")
	<div class="alert alert-success alert-styled-left alert-arrow-left alert-component">
			<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
			<h6 class="alert-heading text-semibold">Success</h6>
			{{Session::get('notification-message')}}
	</div>
@endif
@if(Session::get('notification-status')=="failed")
	<div class="alert alert-danger alert-styled-left alert-arrow-left alert-component">
			<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
			<h6 class="alert-heading text-semibold">Oops!</h6>
			{{Session::get('notification-message')}}
	</div>
@endif