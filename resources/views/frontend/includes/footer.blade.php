<footer>
   
    <div class="footer-bottom">
        <div class="container">
            <div class="col-sm-12 text-center">
            	<p style="margin: 25px;">
            	<a href="{{route('frontend.index')}}">Home</a>
            	
            	<a href="{{route('frontend.about')}}">About</a>
            	
            	<a href="{{route('frontend.product')}}">Products</a>
            	
            	<a href="{{route('frontend.news')}}">News</a>
            	
            	<a href="{{route('frontend.careers')}}">Careers</a>
            	
            	<a href="{{route('frontend.contact')}}">Contact us</a>
            	
            	</p>
            	<p style="margin-bottom: 0;"> &copy; LOTTE CONFECTIONERY PILIPINAS CORPORATION <span> | </span> ALL RIGHTS RESERVED <span> | </span> POWERED BY HIGHLY SUCCEED</p>
            </div>
        </div>
    </div>
    <!--/.footer-bottom--> 
</footer>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script src="{{ asset('frontend/plugins/fakeloader/fakeLoader.min.js?v=1.1')}}"></script>
<script>
    $(document).ready(function(){
        $(".fakeloader").fakeLoader({
            timeToHide:2500,
            timeToHide:3500,
            bgColor:"#fff",
            spinner:"spinner7",
            zIndex: '1500',
        });
    });
</script>
<script type="text/javascript" src="{{ asset('frontend/bootstrap-3.3.6-dist/js/bootstrap.min.js')}}"></script> 


<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>