
   <meta name="viewport" content="width=device-width, initial-scale=1">
<!-- CSS
  ================================================== -->
	<link rel="stylesheet" href="{{ asset('frontend/css/royal_preloader.css')}}">

	<link rel="stylesheet" href="{{ asset('frontend/font-awesome-4.6.3/css/font-awesome.css')}}" />
  	<link rel="stylesheet" href="{{ asset('frontend/bootstrap-3.3.6-dist/css/bootstrap.min.css')}}">

	<!-- Syntax Highlighter -->

  	<link rel="stylesheet" href="{{ asset('frontend/css/main.css?v=1.12')}}">
  	<link rel="stylesheet" href="{{ asset('frontend/plugins/fakeloader/fakeLoader.css?v=1.3')}}">
   	<script src="{{ asset('frontend/plugins/fakeloader/vendor/modernizr-2.6.2-respond-1.1.0.min.js')}}"></script>