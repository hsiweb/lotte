<header>
  
<div class="container-fluid">
    <!-- Second navbar for categories -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="{{url('')}}"><img src="{{asset('frontend/img/lotte-logo.png')}}" alt=""></a>
        </div>
    
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="navbar-collapse-1">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="{{route('frontend.index')}}">HOME</a></li>
            <li><a href="{{route('frontend.about')}}">ABOUT</a></li>
            <li><a href="{{route('frontend.product')}}">PRODUCTS</a></li>
            <li><a href="{{route('frontend.news')}}">NEWS</a></li>
            <li><a href="{{route('frontend.careers')}}">CAREERS</a></li>
            <li><a href="{{route('frontend.contact')}}">CONTACT US</a></li>
            
          </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav><!-- /.navbar -->
</div><!-- /.container-fluid -->
</header>
