<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title> Lotte | Careers </title>
	@include('frontend.includes.styles')
</head>
<body>
	
    <div class="fakeloader"></div>
	@include('frontend.includes.header')

	<div class="banner" style=" margin-top:80px;">
		<div class="container-fluid">
			<div class="col-sm-10 col-sm-offset-1 text-center txt-white">
				<h1 class="career-head">WANT TO JOIN OUR TEAM?</h1>
			</div>
		</div>
	</div>

	<div class="aboutContent">
		@foreach($careers as $index=>$info)
		<img src="{{asset($info->directory.'/'.$info->filename)}}" alt="" width="100%">
		@endforeach
	</div>
	<div class="banner" style="background-color:#dd2f3c!important; padding: 15px 0 ;">
		<h2 class="txt-white text-center bs" style="font-weight: 400;"></h2>
	</div>
	<div class="container-fluid">
			<div class="col-sm-12 text-center" style="margin-bottom: 50px;padding: 10% 0;">
				<h1 class="moment" style="color: #000;">Passionate about creating sweet and delightful experiences? </h1>
				<p class="h1" style="font-size: 32px;text-transform: inherit;">Send your CV/resume to <a href="mailto:info@lottepilipinas.com" style="color: #dd2f3c;">info@lottepilipinas.com</a>! </p>
			</div>
	</div>

	@include('frontend.includes.footer')
</body>
</html>