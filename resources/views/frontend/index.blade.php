<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title> Lotte | Home </title>
	@include('frontend.includes.styles')
</head>
<body>
	
    <div class="fakeloader"></div>
	@include('frontend.includes.header')

	<div class="home" style="background-image: none!important;">
			<div class="content">
				@foreach($header as $index=>$info)
				<img src="{{asset($info->directory.'/'.$info->filename)}}" alt="" width="100%">
				@endforeach
			</div>
	</div>

	@include('frontend.includes.footer')
</body>
</html>