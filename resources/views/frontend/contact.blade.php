<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title> Lotte | Contact </title>
	@include('frontend.includes.styles')
</head>
<body>
	
    <div class="fakeloader"></div>
	@include('frontend.includes.header')

	<div class="wrapper">
		
	<div class="banner" style=" margin-top:80px;">
		<div class="container-fluid">
			<div class="col-sm-10 col-sm-offset-1 text-center txt-white">
				<h1 class="career-head">GET IN TOUCH WITH US!</h1>
			</div>
		</div>
	</div>

	<div class="container contact">
    <div class="row">
      <div class="col-sm-8 t-gray">
        <div class="col-sm-12">
          <div class="row">
            <form action="" method="POST">
            @if(Session::get('notification-status') == "success")
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-component">
              <button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
              {!! Session::get('notification-msg') !!}
            </div>

            @endif
            <input type="hidden" name="_token" value="{{csrf_token()}}">
              <div class="col-sm-6">
              <div class="input {{$errors->first('name') ? 'has-error' : NULL}}">
                Name <br>
                <input type="text" name="name" value="{{old('name')}}" required>
                @if($errors->first('name'))
                <span class="help-block">{{$errors->first('name')}}</span>
                @endif
              </div>
              <div class="input {{$errors->first('email') ? 'has-error' : NULL}}">
                  E-mail Address<br>
                <input type="email" name="email"  value="{{old('email')}}" required>
                @if($errors->first('email'))
                <span class="help-block">{{$errors->first('email')}}</span>
                @endif
                </div>
                <div class="input {{$errors->first('contact_no') ? 'has-error' : NULL}}">
                  Contact No. <br>
                  <input type="text" name="contact_no" value="{{old('contact_no')}}" required>
                @if($errors->first('contact_no'))
                <span class="help-block">{{$errors->first('contact_no')}}</span>
                @endif
                </div>
                <div class="text-left" style="margin-top: 100px;">
                  <p class="h4">Captcha</p>
                  <p class="input"><input type="text" value="{{Str::random($length = 7)}}" name="captcha" style="width: 40%;" readonly> 
                  <input type="text" required style="width: 50%; margin-left: 20px" name="captcha_confirmation"></p>
                  @if($errors->first('captcha'))
                  <span class="help-block">{{$errors->first('captcha')}}</span>
                  @else
                  <span class="help-block">Case Sensitive</span>
                  @endif
                </div>
              </div>
            <div class="col-sm-6">
              <div class="input {{$errors->first('subject') ? 'has-error' : NULL}}">
                Subject <br>
              <input type="text" name="subject" value="{{old('subject')}}" required>
              @if($errors->first('subject'))
              <span class="help-block">{{$errors->first('subject')}}</span>
              @endif
              </div>
              <div class="input {{$errors->first('message') ? 'has-error' : NULL}}">Message</div>
              <textarea name="message" id="" rows="10" class="input" style="border-radius: 5px;" value="{{old('message')}}" required>{{old('message')}}</textarea>
              @if($errors->first('message'))
              <span class="help-block">{{$errors->first('message')}}</span>
              @endif
              <div class="text-right"><button type="submit" class="btn btn-danger">SEND</button></div>
            </div>
            </form>
            
          </div>
        </div>
      </div>
      <div class="col-sm-4 address">
  	    <span class="fa fa-globe fa-2x"></span> <p>OUR OFFICE <br><br>
  	    LOTTE CONFECTIONERY <br>
  	    PILIPINAS CORPORATION
  	    </p>

  	    <p>
         Unit 1702, Hanston Square Bldg.,<br>
        17 San Miguel Ave., Ortigas Center <br>
        Pasig City, Metro Manila, Philippines 
        </p>
        <p>Tel No.: +632-470-5272 <br>
        Fax No.: +632-654-2482 <br>
        E-mail: <a href="mailto:info@lottepilipinas.com">info@lottepilipinas.com</a>
        </p>
      </div>
    </div>
  </div>
	@include('frontend.includes.footer')
</body>
</html>