<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title> Lotte | Home </title>
	@include('frontend.includes.styles')
</head>
<body>
	
    <div class="fakeloader"></div>
	@include('frontend.includes.header')
	
	@foreach($career as $index=>$info)
	<div class="banner" style=" margin-top:80px;">
		<div class="container-fluid">
			<div class="col-sm-10 col-sm-offset-1 text-center txt-white">
				<h1 class="career-head" style="font-size: 40px">{{$info->position}}</h1>
			</div>
		</div>
	</div>

	<div class="container text-justify railway" style="padding: 30px 0;" >
		<p>{!!$info->position_description!!}</p>
	</div>
	<hr style="border-bottom: 5px solid #414042; border-top: none; margin-bottom: 0">
	<div class="container">
		<div class="col-sm-10 col-sm-offset-1" style="padding:20px 0;color: #000;">
			<div class="row">
				<div class="col-sm-6" style="vertical-align: middle;">
					<div class="detail-middle text-center">
						<hr style="border-bottom: 2px solid #414042; border-top: none; width: 80%; margin-bottom: 20px;">
						<div class="details text-center"  style="padding: 50px;">
							{!!$info->more_info!!}
						</div>
						<hr style="border-bottom: 2px solid #414042; border-top: none; width: 80%; margin-top: 20px;">
						<a href="mailto:info@lottepilipinas.com"><button class="btn button-dark" style="margin-top: 20px;">Apply now</button></a>
					</div>
				</div>
				<div class="col-sm-6 career-list">
					<h3 class="railway-b" style="margin-top: 10px;">RESPONSIBILITIES</h3>
						{!!$info->responsibilities!!}
					<h3 class="railway-b">REQUIREMENTS</h3>
						{!!$info->requirements!!}
				</div>
			</div>
		</div>
	</div>
	@endforeach

	@include('frontend.includes.footer')
</body>
</html>