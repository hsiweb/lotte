<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title> Lotte | Products </title>
	@include('frontend.includes.styles')

	<link rel="stylesheet" href="{{ asset('plugins/swiper/dist/css/swiper.min.css')}}">
</head>
<body style=" background-color:#efefef;">
	
    <div class="fakeloader"></div>
	@include('frontend.includes.header')
	@foreach($products as $index=>$info)
		<img src="{{asset($info->directory.'/'.$info->filename)}}" alt="" width="100%" style="margin-top: 80px;">
	@endforeach
	<div class="banner" style="background-color:#dd2f3c!important; letter-spacing: 5px; padding: 15px 0; min-height: 90px!important;">
		<h3 class="txt-white text-center bs">PRODUCTS</h3>
	</div>
	<div class="container" style=" background-color:#efefef;"">
		<div class="col-sm-10 col-sm-offset-1">
			<h1 class="txt-white text-center pt" style="color: #414042">BISCUITS</h1>
			<div class="row">
				<div class="col-sm-4 text-center pads">
					<div class="product" style="    min-height: 500px;">

						<h1>PEPERO</h1>
						
						<div id="carousel-3" class="carousel slide" data-ride="carousel">
									  
						  <!-- Wrapper for slides -->
						  <div class="carousel-inner" role="listbox">
						  @foreach($pepero as $index=>$info)
						    <div class="item {{++$pepero_counter==1? 'active' : NULL}} ">
						      <img src="{{ asset($info->directory.'/'.$info->filename.'?v=1.1')}}" alt="" class="item-center" width="50%">
						      <div class="carousel-caption">
						        <p style="padding-top:20px;">
								<strong>{{$info->product_name}}</strong></p>
								<hr>
								<p>
								 {{$info->product_details}}
								</p>
						      </div>
						    </div>
						  @endforeach
						  </div>
						  <!-- Indicators -->
						  <!-- Controls -->
						  <a class="left carousel-control" href="#carousel-3" role="button" data-slide="prev">
						    <span class="fa fa-caret-left" style="left: 2px"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="right carousel-control" href="#carousel-3" role="button" data-slide="next">
						    
						    <span class="fa fa-caret-right" style="right: 2px;"></span>
						    <span class="sr-only">Next</span>
						  </a>
						</div>
						
					</div>
				</div>
				<div class="col-sm-4 text-center pads">
					<div class="product" style="    min-height: 500px;">
						<h1>TOPPO</h1>
						  <div id="carousel-5" class="carousel slide" data-ride="carousel">
								  
								  <!-- Wrapper for slides -->
								  <div class="carousel-inner" role="listbox">
								  @foreach($toppo as $index=>$info)
								    <div class="item {{++$toppo_counter == 1 ? 'active' : NULL}}">
								      <img src="{{ asset($info->directory.'/'.$info->filename.'?v=1.2')}}" alt="" class="item-center" width="50%">
								      <div class="carousel-caption">
								       <p style="padding-top:20px;">
										<strong>{{$info->product_name}}</strong></p>
										<hr>
										<p>{{$info->product_details}}
									  </p>
								      </div>
								    </div>
								   @endforeach
								  </div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 text-center pads">
					<div class="product" style="    min-height: 500px;">
						<h1>KOALA'S MARCH</h1>
						  
						<div id="carousel-4" class="carousel slide" data-ride="carousel">
								  
								  <!-- Wrapper for slides -->
								  <div class="carousel-inner" role="listbox">
								  @foreach($koala as $index=>$info)
								    <div class="item {{++$koala_counter == 1 ? 'active'  : NULL}}">
								      <img src="{{ asset($info->directory.'/'.$info->filename.'?v=1.2')}}" alt="" class="item-center" width="50%">
								      <div class="carousel-caption">
								       <p style="padding-top:20px;">
										<strong>{{$info->product_name}}</strong></p>
										<hr>
										<p>{{$info->product_details}}
									  </p>
								      </div>
								    </div>
								   @endforeach
								  </div>
								  <!-- Indicators -->
								  
								  <!-- Controls -->
								  <a class="left carousel-control" href="#carousel-4" role="button" data-slide="prev">
								    <span class="fa fa-caret-left" style="left: 2px"></span>
								    <span class="sr-only">Previous</span>
								  </a>
								  <a class="right carousel-control" href="#carousel-4" role="button" data-slide="next">
								    <span class="fa fa-caret-right" style="right: 2px;"></span>
								    <span class="sr-only">Next</span>
								  </a>
						</div>
					</div>
				</div>
		  	</div>

			
			<h1 class="txt-white text-center pt" style="color: #414042">CAKES</h1>
		  	<div class="row">
				<div class="col-sm-4 col-sm-offset-2 text-center pads">
					<div class="product">
						<h1>Custard Cake</h1>
						 <div id="carousel-6" class="carousel slide" data-ride="carousel">
								  
								  <!-- Wrapper for slides -->
								  <div class="carousel-inner" role="listbox">
								  @foreach($custard_cake as $index=>$info)
								    <div class="item {{++$custard_cake_counter == 1 ? 'active'  : NULL}}">
								      <img src="{{ asset($info->directory.'/'.$info->filename.'?v=1.2')}}" alt="" class="item-center" width="65%">
								      <div class="carousel-caption">
								       <p style="padding-top:20px;">
										<strong>{{$info->product_name}}</strong></p>
										<hr>
										<p>{{$info->product_details}}
									  </p>
								      </div>
								    </div>
								   @endforeach
								  </div>
						</div>
					</div>
				</div>
				<div class="col-sm-4 text-center pads">
					<div class="product">
						<h1>Choco Pie</h1>
						  <div id="carousel-7" class="carousel slide" data-ride="carousel">
								  
								  <!-- Wrapper for slides -->
								  <div class="carousel-inner" role="listbox">
								  @foreach($choco_pie as $index=>$info)
								    <div class="item {{++$choco_pie_counter == 1 ? 'active'  : NULL}}">
								      <img src="{{ asset($info->directory.'/'.$info->filename.'?v=1.2')}}" alt="" class="item-center" width="65%">
								      <div class="carousel-caption">
								       <p style="padding-top:20px;">
										<strong>{{$info->product_name}}</strong></p>
										<hr>
										<p>{{$info->product_details}}
									  </p>
								      </div>
								    </div>
								   @endforeach
								  </div>
						</div>
					</div>
				</div>
		  	</div>

			
			<h1 class="txt-white text-center pt" style="color: #414042">GUMS</h1>
		  	<div class="row" style="margin-bottom: 50px;">
				<div class="col-sm-4 col-sm-offset-2 text-center pads">
					<div class="product">
						<h1>Xylitol</h1>
						<div id="carousel-2" class="carousel slide" data-ride="carousel">
								  
								  <!-- Wrapper for slides -->
								<div class="carousel-inner" role="listbox">
								  @foreach($xylitol as $index=>$info)
								    <div class="item {{++$xylitol_counter == 1 ? 'active'  : NULL}}">
								      <img src="{{ asset($info->directory.'/'.$info->filename.'?v=1.2')}}" alt="" class="item-center" width="65%">
								      <div class="carousel-caption">
								       <p style="padding-top:20px;">
										<strong>{{$info->product_name}}</strong></p>
										<hr>
										<p>{{$info->product_details}}
									  </p>
								      </div>
								    </div>
								   @endforeach
								  </div>

								    
								  <!-- Indicators -->
								  
								  <!-- Controls -->
								  <a class="left carousel-control" href="#carousel-2" role="button" data-slide="prev">
								    <span class="fa fa-caret-left" style="left: 2px"></span>
								    <span class="sr-only">Previous</span>
								  </a>
								  <a class="right carousel-control" href="#carousel-2" role="button" data-slide="next">
								    <span class="fa fa-caret-right" style="right: 2px;"></span>
								    <span class="sr-only">Next</span>
								  </a>
						</div>
					</div>
				</div>
				<div class="col-sm-4 text-center pads">
					<div class="product">
						<h1>Fusen No Mi</h1>
						  <!-- <img src="img/Fusen No Mi - Primary (Cola Lemon).png" alt="" class="item-center" width="30%">
						  <p>
							<strong>Available in other flavors:</strong><br>
							<ul style="list-style: none; padding-left: 0; text-transform: uppercase;">
								<li>Strawberry</li>
								<li>Blueberry</li>
								<li>Lemon Cola</li>
							</ul>
						  </p> -->
				      	<div id="carousel-1" class="carousel slide" data-ride="carousel">
						  
						  <!-- Wrapper for slides -->
						  <div class="carousel-inner" role="listbox">
						  	@foreach($fusen_no_mi as $index=>$info)
						  	<div class="item {{++$fusen_no_mi_counter == 1 ? 'active'  : NULL}}">
						  		<img src="{{ asset($info->directory.'/'.$info->filename.'?v=1.2')}}" alt="" class="item-center" width="30%">
						  		<div class="carousel-caption">
						  			<p style="padding-top:20px;">
						  				<strong>{{$info->product_name}}</strong></p>
						  				<hr>
						  				<p>{{$info->product_details}}
						  				</p>
						  			</div>
						  		</div>
						  		@endforeach
						  	</div>
						  <!-- Indicators -->
						  
						  <!-- Controls -->
						  <a class="left carousel-control" href="#carousel-1" role="button" data-slide="prev">
						    <span class="fa fa-caret-left" style="left: 2px"></span>
						    <span class="sr-only">Previous</span>
						  </a>
						  <a class="right carousel-control" href="#carousel-1" role="button" data-slide="next">
						    <span class="fa fa-caret-right" style="right: 2px;"></span>
						    <span class="sr-only">Next</span>
						  </a>
						</div>

					</div>
				</div>
		  	</div>
		</div>
	</div>
	
	@include('frontend.includes.footer')

    <script type="text/javascript">
    	$('.carousel').carousel({
		  interval: 0
		})
    </script>

</body>
</html>