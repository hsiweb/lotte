<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
	<title>Lotte</title>
	<style type="text/css">
	.ReadMsgBody { width: 100%; background-color: #ffffff; }
	.ExternalClass { width: 100%; background-color: #ffffff; }
	.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }
	html { width: 100%; }
	body { -webkit-text-size-adjust: none; -ms-text-size-adjust: none; }
	body { margin: 0; padding: 0; }
	table { border-spacing: 0; }
	img { display: block !important; }
	table td { border-collapse: collapse; }
	.yshortcuts a { border-bottom: none !important; }
	a { color: #cf4747; text-decoration: none; }
	@media only screen and (max-width: 640px) {
		body { width: auto !important; }
		table[class="table600"] { width: 450px !important; text-align: center !important; }
	}
	@media only screen and (max-width: 479px) {
		body { width: auto !important; }
		table[class="table600"] { width: 290px !important; }
	}
	</style>

</head>

<body>
	<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#ffffff">
		<tr>
			<td valign="top">
				<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">

					<!-- quote -->
					<tr>
						<td>
							<table class="table600" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
								<tr>
									<td height="30"></td>
								</tr>

								{{--
								<tr>
									<td align="center">
										<img src="{{$message->embed(asset('backoffice/images/logo.png'))}}" width="100" height="100" alt="krops-icon" />
									</td>
								</tr>
								--}}

								<tr>
									<td height="15"></td>
								</tr>

								<!-- title -->
								<tr valign="top">
									<td align="center" style="font-family: 'Century Gothic', arial, sans-serif; font-size:26px; color:#4d4d4d; font-weight:bold;">Lotte Contact Inquiry:</td>
								</tr>
								<!-- end title -->

								<tr>
									<td height="10"></td>
								</tr>

								<!-- content -->
								<tr valign="top">
									<td align="" style="font-family: 'Century Gothic', arial, sans-serif; font-size:26px; font-size:16px; color:#999999; line-height:30px;">
										<ul style="list-style: none;">
											<li>Name: {!!$name!!}</li>
											<li>Subject: {!!$subject!!}</li>
											<li>Email Address: {!!$email!!}</li>
											<li>Contact #: {!!$contact_no!!}</li>
											<li>Message: {!!$msg!!}</li>
										</ul>
									</td>
								</tr>
								<!-- end contnet -->

								<tr>
									<td height="15"></td>
								</tr>
								<tr>
									<td align="center" style="font-family: 'Century Gothic', arial, sans-serif; font-size:26px; font-size:16px; color:#cf4747; font-weight:bold;">
								</tr>
								<tr>
									<td height="30"></td>
								</tr>
							</table>
						</td>
					</tr>
					<!-- end quote -->
				</table>
			</td>
		</tr>
	</table>
</body>

</html>