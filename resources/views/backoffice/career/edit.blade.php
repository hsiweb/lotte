@extends('backoffice._template.main')
@section('content')
<!-- Page header -->
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-briefcase3"></i> <span class="text-semibold">Career</span> - Edit a career.</h4>
		</div>
		<div class="heading-elements">
			<div class="heading-btn-group">
				<a href="{{route('backoffice.career.index')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack text-primary"></i><span>All Data</span></a>
				<a href="{{route('backoffice.career.create')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack-plus text-primary"></i><span>Create New</span></a>
			</div>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{route('backoffice.dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="{{route('backoffice.career.index')}}">Career</a></li>
			<li class="active">Create</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
	<form id="target" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">Career Details</h5>
				<div class="heading-elements">
					<ul class="icons-list">
	            		<li><a data-action="collapse"></a></li>
	            		<!-- <li><a data-action="reload"></a></li> -->
	            		<!-- <li><a data-action="close"></a></li> -->
	            	</ul>
	        	</div>
			</div>

			<div class="panel-body">
				
				<p class="content-group-lg">Below are the general information for this career.</p>

				<div class="form-group {{$errors->first('position') ? 'has-error' : NULL}}">
					<label for="position" class="control-label col-lg-2 text-right">career Title <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="position" id="position" placeholder="" maxlength="100" value="{{old('position',$career->position)}}">
						@if($errors->first('position'))
						<span class="help-block">{{$errors->first('position')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('position_description') ? 'has-error' : NULL}}">
					<label for="title" class="control-label col-lg-2 text-right">career Content <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<textarea id="bodyField" name="position_description">{{old('position_description',$career->position_description)}}</textarea>
						@ckeditor('bodyField')
						@if($errors->first('position_description'))
						<span class="help-block">{{$errors->first('position_description')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('responsibilities') ? 'has-error' : NULL}}">
					<label for="responsibilities" class="control-label col-lg-2 text-right">Responsibilities <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<textarea id="bodyField2" name="responsibilities">{{old('responsibilities',$career->responsibilities)}}</textarea>
						@ckeditor('bodyField2')
						@if($errors->first('responsibilities'))
						<span class="help-block">{{$errors->first('responsibilities')}}</span>
						@endif
					</div>
				</div>
				
				<div class="form-group {{$errors->first('requirements') ? 'has-error' : NULL}}">
					<label for="requirements" class="control-label col-lg-2 text-right">Requirements<span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<textarea id="bodyField3" name="requirements">{{old('requirements',$career->requirements)}}</textarea>
						@ckeditor('bodyField3')
						@if($errors->first('requirements'))
						<span class="help-block">{{$errors->first('requirements')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('more_info') ? 'has-error' : NULL}}">
					<label for="more_info" class="control-label col-lg-2 text-right">More Information<span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<textarea id="bodyField4" name="more_info">{{old('more_info',$career->more_info)}}</textarea>
						@ckeditor('bodyField4')
						@if($errors->first('more_info'))
						<span class="help-block">{{$errors->first('more_info')}}</span>
						@endif
					</div>
				</div>
				
			</div>
		</div>

		<div class="content-group">
			<div class="text-left">
				<button id="save" type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Saving ..." class="btn btn-primary btn-raised btn-lg btn-loading">Save</button>
				&nbsp;
				<a type="button" class="btn btn-default btn-raised btn-lg" href="{{route('backoffice.career.index')}}">Cancel</a>
			</div>
		</div>
	</form>
	@include('backoffice._includes.footer')
</div>
<!-- /content area -->
@stop
@section('modals')
@stop
@section('page-styles')
@stop
@section('page-scripts')
@include('backoffice._includes.page-jgrowl')
<script type="text/javascript" src="{{asset('backoffice/js/pages/components_popups.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/spin.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/ladda.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/styling/uniform.min.js')}}"></script>

<!-- Select2 -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/custom/select2.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/pages/form_inputs.js')}}"></script>


<script type="text/javascript">
	$(function(){

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.btn-loading').click(function () {
	        var btn = $(this);
	        btn.button('loading');
	    });

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.select').each(function(){
	    	$id = "#" + $(this).attr('id') + " option:first";
	    	$($id).prop('disabled',1);
	    });
	});
</script>
@stop