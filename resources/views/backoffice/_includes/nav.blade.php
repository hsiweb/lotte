<li class="{{Helper::is_active($routes['dashboard'])}}">
	<a href="{{route('backoffice.dashboard')}}"><i class="icon-home4"></i> <span>Dashboard</span></a>
	<li class="navigation-header"><span>Web Content</span> <i class="icon-menu" title="Web Content"></i></li>

	<li class="{{Helper::is_active($routes['header'],'open')}}">
		<a href="#"><i class="icon-grid6"></i> <span>Header</span></a>
		<ul>
			<li class="{{Helper::is_active([$routes['header'][0]])}}"><a href="{{route('backoffice.header.index')}}">Record Data</a></li>
			<li class="{{Helper::is_active([$routes['header'][1]])}}"><a href="{{route('backoffice.header.create')}}">Create New</a></li>
			<li class="{{Helper::is_active([$routes['header'][4]])}}"><a href="{{route('backoffice.header.deletes')}}">Deleted</a></li>
		</ul>
	</li>

	<li class="{{Helper::is_active($routes['image_slider'],'open')}}">
		<a href="#"><i class="icon-images2"></i> <span>Image Slider</span></a>
		<ul>
			<li class="{{Helper::is_active([$routes['image_slider'][0]])}}"><a href="{{route('backoffice.image_slider.index')}}">Record Data</a></li>
			<li class="{{Helper::is_active([$routes['image_slider'][1]])}}"><a href="{{route('backoffice.image_slider.create')}}">Create New</a></li>
			<li class="{{Helper::is_active([$routes['image_slider'][4]])}}"><a href="{{route('backoffice.image_slider.deletes')}}">Deleted</a></li>
		</ul>
	</li>

	<li class="{{Helper::is_active($routes['product'],'open')}}">
		<a href="#"><i class="icon-bag"></i> <span>Product</span></a>
		<ul>
			<li class="{{Helper::is_active([$routes['product'][0]])}}"><a href="{{route('backoffice.product.index')}}">Record Data</a></li>
			<li class="{{Helper::is_active([$routes['product'][1]])}}"><a href="#">Create New</a>
				<ul>
					<li class="{{Helper::is_active([$routes['product']])}}"><a href="{{route('backoffice.product.create')}}/biscuits">Biscuits</a></li>
					<li class="{{Helper::is_active([$routes['product']])}}"><a href="{{route('backoffice.product.create')}}/cakes">Cakes</a>
					</li>
					<li class="{{Helper::is_active([$routes['product']])}}"><a href="{{route('backoffice.product.create')}}/gums">Gums</a></li>
				</ul>
			</li>
			<li class="{{Helper::is_active([$routes['product'][4]])}}"><a href="{{route('backoffice.product.deletes')}}">Deleted</a></li>
		</ul>
	</li>

	<li class="{{Helper::is_active($routes['news'],'open')}}">
		<a href="#"><i class="icon-newspaper2"></i> <span>News</span></a>
		<ul>
			<li class="{{Helper::is_active([$routes['news'][0]])}}"><a href="{{route('backoffice.news.index')}}">Record Data</a></li>
			<li class="{{Helper::is_active([$routes['news'][1]])}}"><a href="{{route('backoffice.news.create')}}">Create New</a></li>
			<li class="{{Helper::is_active([$routes['news'][4]])}}"><a href="{{route('backoffice.news.deletes')}}">Deleted</a></li>
		</ul>
	</li>

	<li class="{{Helper::is_active($routes['career'],'open')}}">
		<a href="#"><i class="icon-briefcase3"></i> <span>Careers</span></a>
		<ul>
			<li class="{{Helper::is_active([$routes['career'][0]])}}"><a href="{{route('backoffice.career.index')}}">Record Data</a></li>
			<li class="{{Helper::is_active([$routes['career'][1]])}}"><a href="{{route('backoffice.career.create')}}">Create New</a></li>
			<li class="{{Helper::is_active([$routes['career'][4]])}}"><a href="{{route('backoffice.career.deletes')}}">Deleted</a></li>
		</ul>
	</li>

	<li class="{{Helper::is_active($routes['tagline'],'open')}}">
		<a href="#"><i class="icon-file-text"></i> <span>Taglines</span></a>
		<ul>
			<li class="{{Helper::is_active([$routes['tagline'][0]])}}"><a href="{{route('backoffice.tagline.index')}}">Record Data</a></li>
			<li class="{{Helper::is_active([$routes['tagline'][1]])}}"><a href="{{route('backoffice.tagline.create')}}">Create New</a></li>
			<li class="{{Helper::is_active([$routes['tagline'][4]])}}"><a href="{{route('backoffice.tagline.deletes')}}">Deleted</a></li>
		</ul>
	</li>

	<li class="navigation-header"><span>Contact Inquiries</span> <i class="icon-question-mark" title="Contact Inquiries"></i></li>
	<li class="{{Helper::is_active([$routes['contact'][0]])}}">
		<a href="{{route('backoffice.contact.index')}}"><i class="icon-info22"></i> <span>Contact</span></a>
	</li>
</li>