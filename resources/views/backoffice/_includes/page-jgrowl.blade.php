@if(Session::has('notification-status'))
<script type="text/javascript" src="{{asset('backoffice/js/plugins/notifications/jgrowl.min.js')}}"></script>
<script type="text/javascript">

    var type = "";
    var title = "";
    var theme = "";

    @if(Session::get('notification-status') == "failed")
        type = "danger";
        title = "{{Session::get('notification-title','Failed')}}";
        theme = "alert alert-danger alert-styled-left alert-arrow-left alert-component"
    @endif

    @if(Session::get('notification-status') == "success")
        type = "success";
        title = "{{Session::get('notification-title','Success')}}";
        theme = "alert alert-success alert-styled-left alert-arrow-left alert-component"
    @endif

    @if(Session::get('notification-status') == "warning")
        type = "warning";
        title = "{{Session::get('notification-title','Warning')}}";
        theme = "alert alert-warning alert-styled-left alert-arrow-left alert-component"
    @endif

    @if(Session::get('notification-status') == "info")
        type = "info";
        title = "{{Session::get('notification-title','Information')}}";
        theme = "alert alert-info alert-styled-left alert-arrow-left alert-component"
    @endif

    $(function(){
        // Custom style
        $.jGrowl("{{Session::get('notification-msg')}}", {
            position: 'top-right',
            header: title,
            theme: theme,
            life: 50000,
        });
    })
</script>
@endif