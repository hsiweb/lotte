@if(Session::has('notification-status'))
	@if(Session::get('notification-status') == "failed")
	<div class="alert alert-danger alert-styled-left alert-arrow-left alert-component">
		<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
		<h6 class="alert-heading text-semibold">{{Session::get('notification-title',"Failed")}}</h6>
		{!! Session::get('notification-msg') !!}
	</div>
	@endif

	@if(Session::get('notification-status') == "success")
	<div class="alert alert-success alert-styled-left alert-arrow-left alert-component">
		<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
		<h6 class="alert-heading text-semibold">{{Session::get('notification-title',"Success")}}</h6>
		{!! Session::get('notification-msg') !!}
	</div>

	@endif

	@if(Session::get('notification-status') == "warning")
	<div class="alert alert-warning alert-styled-left alert-arrow-left alert-component">
		<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
		<h6 class="alert-heading text-semibold">{{Session::get('notification-title',"Warning")}}</h6>
		{!! Session::get('notification-msg') !!}
	</div>
	@endif

	@if(Session::get('notification-status') == "info")
	<div class="alert alert-info alert-styled-left alert-arrow-left alert-component">
		<button type="button" class="close" data-dismiss="alert"><span>&times;</span><span class="sr-only">Close</span></button>
		<h6 class="alert-heading text-semibold">{{Session::get('notification-title',"Information")}}</h6>
		{!! Session::get('notification-msg') !!}
	</div>
	@endif
@endif