@extends('backoffice._template.main')
@section('content')
<!-- Page header -->
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-info22"></i> <span class="text-semibold">Contact infos</span> - List of all the contact infos stored in this application.</h4>
		</div>
		<div class="heading-elements">
			<div class="heading-btn-group">
				
			</div>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{route('backoffice.dashboard')}}"><i class="icon-header2 position-left"></i> Contact</a></li>
			<li class="active">Contact infos</li>
		</ul>
	</div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Record Data</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			Here are the list of <code>all contact infos</code> in this application. <strong>Manage each row by clicking the action button on the far right portion of the table.</strong>
		</div>

		<table class="table datatable-basic table-hover" id="target-table">
			<thead>
				<tr>
					<th class="text-center" width="5%">#</th>
					<th class="text-center">Name</th>
					<th class="text-center">Email</th>
					<th class="text-center">Contact No.</th>
					<th class="text-center">Subject</th>
					<th class="text-center">Message</th>
					<th class="text-center">Last Modified</th>
					<th class="text-center" width="7%"></th>
				</tr>
			</thead>
			<tbody>
			@foreach($contact as $index => $info)
				<tr>
					<td class="text-center">{{++$index}}</td>
					<td class="text-center">{{Str::limit($info->name,$limit="15",$end="...")}}</td>
					<td class="text-center">{{Str::limit($info->email,$limit="15",$end="...")}}</td>
					<td class="text-center">{{Str::limit($info->contact_no,$limit="15",$end="...")}}</td>
					<td class="text-center">{{Str::limit($info->subject,$limit="20",$end="...")}}</td>
					<td class="text-center">{{Str::limit($info->message,$limit="30",$end="...")}}</td>
					<td class="text-center" title="{{$info->updated_at}}">{{$info->last_modified()}}</td>
					<td class="text-center">
						<div class="btn-group">
							    <button type="button" class="btn btn-primary btn-xs btn-raised dropdown-toggle" data-toggle="dropdown">action <span class="caret"></span></button>
							    <ul class="dropdown-menu dropdown-menu-right">
								<li><a href="#" class="action-delete" data-url="" data-toggle="modal" data-target="#confirm-delete"><i class="icon-eye"></i> View</a></li>
							</ul>
						</div>
					</td>
				</tr>
				@section('modals')
				<!-- Basic modal -->
				<div id="confirm-delete" class="modal fade">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h5 class="modal-title">Contact inquiry details</h5>
							</div>

							<div class="modal-body">

								<label class="text-semibold">From:   {{$info->name}}</label>
								<br>
								<label class="text-semibold">Contact No:   {{$info->contact_no}}</label>
								<br>
								<label class="text-semibold">Subject:   {{$info->subject}}</label>
								<hr>
								<h6 class="text-semibold">Message</h6>
								<p>{{$info->message}}</p>
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
				<!-- /basic modal -->
				@stop
			@endforeach
				
			</tbody>
		</table>
	</div>

	@include('backoffice._includes.footer')

</div>
<!-- /content area -->
@stop

@section('page-styles')
@stop
@section('page-scripts')
@include('backoffice._includes.page-jgrowl')
<script type="text/javascript" src="{{asset('backoffice/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/custom/datatable.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript">
	$(function(){

		$('#target-table').delegate('.action-delete', "click", function(){
			$("#btn-confirm-delete").attr({"href" : $(this).data('url')});
		});
	});
</script>
@stop