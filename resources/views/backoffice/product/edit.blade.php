@extends('backoffice._template.main')
@section('content')
<!-- Page Product -->
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-grid6"></i> <span class="text-semibold">Product</span> - Edit a Product.</h4>
		</div>
		<div class="heading-elements">
			<div class="heading-btn-group">
				<a href="{{route('backoffice.image_slider.index')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack text-primary"></i><span>All Data</span></a>
				<a href="{{route('backoffice.image_slider.create')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack-plus text-primary"></i><span>Create New</span></a>
			</div>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{route('backoffice.dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="{{route('backoffice.image_slider.index')}}">Product</a></li>
			<li class="active">Create</li>
		</ul>
	</div>
</div>
<!-- /page Product -->


<!-- Content area -->
<div class="content">
	<form id="target" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">Product Details</h5>
				<div class="heading-elements">
					<ul class="icons-list">
	            		<li><a data-action="collapse"></a></li>
	            		<!-- <li><a data-action="reload"></a></li> -->
	            		<!-- <li><a data-action="close"></a></li> -->
	            	</ul>
	        	</div>
			</div>

			<div class="panel-body">
				
				<p class="content-group-lg">Below are the general information for this Product.</p>
				<div class="form-group {{$errors->first('type') ? 'has-error' : NULL}}">
					<label for="type" class="control-label col-lg-2 text-right">Type <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
					@if($category=="biscuits")
						{!!Form::select("sub_category_name", $biscuits, old('sub_category_name',$product->sub_category_name), ['id' => "sub_category_name", 'class' => "select col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
						@if($errors->first('sub_category_name'))
						<span class="help-block">{{$errors->first('sub_category_name')}}</span>
						@endif
					@elseif($category=="cakes")
						{!!Form::select("sub_category_name", $cakes, old('sub_category_name',$product->sub_category_name), ['id' => "sub_category_name", 'class' => "select col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
						@if($errors->first('sub_category_name'))
						<span class="help-block">{{$errors->first('sub_category_name')}}</span>
						@endif
					@elseif($category=="gums")
						{!!Form::select("sub_category_name", $gums, old('sub_category_name',$product->sub_category_name), ['id' => "sub_category_name", 'class' => "select col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
						@if($errors->first('sub_category_name'))
						<span class="help-block">{{$errors->first('sub_category_name')}}</span>
						@endif
					@else
						<script type="text/javascript">
						    window.location = "{ url('/'.$category) }";
						</script>
					@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('product_name') ? 'has-error' : NULL}}">
					<label for="title" class="control-label col-lg-2 text-right">Product Name <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="product_name" id="product_name" placeholder="" maxlength="100" value="{{old('product_name',$product->product_name)}}">
						@if($errors->first('product_name'))
						<span class="help-block">{{$errors->first('product_name')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('product_details') ? 'has-error' : NULL}}">
					<label for="title" class="control-label col-lg-2 text-right">Product Details <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="product_details" id="product_details" placeholder="" maxlength="100" value="{{old('product_details',$product->product_details)}}">
						@if($errors->first('product_details'))
						<span class="help-block">{{$errors->first('product_details')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('file') ? 'has-error' : NULL}}">
					<label class="control-label col-lg-2 text-right">Upload thumbnail</label>
					<div class="col-lg-9">
						<input type="file" name="file" class="file-styled-primary">
						@if($errors->first('file'))
						<span class="help-block">{{$errors->first('file')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group">
					@if($product->filename)
					<label class="control-label col-lg-2 text-right">Current thumbnail</label>
					<div class="col-lg-9">
						<img src="{{asset($product->directory.'/resized/'.$product->filename)}}" class="img-responsive img-rounded" width="250" alt="">
					</div>
					@else
					<div class="col-lg-offset-2">
						<label for="" class="pl-10">No thumbnail yet.</label>
					</div>
					@endif
				</div>
				
			</div>
		</div>

		<div class="content-group">
			<div class="text-left">
				<button id="save" type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Saving ..." class="btn btn-primary btn-raised btn-lg btn-loading">Save</button>
				&nbsp;
				<a type="button" class="btn btn-default btn-raised btn-lg" href="{{route('backoffice.image_slider.index')}}">Cancel</a>
			</div>
		</div>
	</form>
	@include('backoffice._includes.footer')
</div>
<!-- /content area -->
@stop
@section('modals')
@stop
@section('page-styles')
@stop
@section('page-scripts')
@include('backoffice._includes.page-jgrowl')
<script type="text/javascript" src="{{asset('backoffice/js/pages/components_popups.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/spin.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/ladda.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/styling/uniform.min.js')}}"></script>

<!-- Select2 -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/custom/select2.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/pages/form_inputs.js')}}"></script>

<!-- Daterange Picker -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/ui/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/pickers/daterangepicker.js')}}"></script>

<!-- CKEditor -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/editors/ckeditor/ckeditor.js')}}"></script>


<script type="text/javascript">
	$(function(){

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.btn-loading').click(function () {
	        var btn = $(this);
	        btn.button('loading');
	    });

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.select').each(function(){
	    	$id = "#" + $(this).attr('id') + " option:first";
	    	$($id).prop('disabled',1);
	    });

	    $('.daterange-single').daterangepicker({ 
			singleDatePicker: true,
			timePicker: true,
			timePickerIncrement: 1,
			locale: {
				format: 'YYYY-MM-DD h:mm A'
			}
		});

	});
</script>
@stop