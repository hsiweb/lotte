<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'send-email' => [
            'App\Laravel\Listeners\SendEmailListener'
        ],
        'forgot-password' => [
            'App\Laravel\Listeners\ForgotPasswordListener'
        ],
        'push-notification' => [
            'App\Laravel\Listeners\PushNotificationListener'
        ],
        'email-notification' => [
            'App\Laravel\Listeners\EmailNotificationListener'
        ],
        'log-user-action' => [
            'App\Laravel\Listeners\LogUserActionListener'
        ],
        'shake-blast' => [
            'App\Laravel\Listeners\ShakeBlastListener'
        ],
        'log-citizen-request' => [
            'App\Laravel\Listeners\LogCitizenRequestListener'
        ],
        'fcm-notification' => [
            'App\Laravel\Listeners\FCMNotificationListener'
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
