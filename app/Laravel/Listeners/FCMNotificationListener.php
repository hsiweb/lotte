<?php 

namespace App\Laravel\Listeners;

use App\Laravel\Events\FCMNotification;

class FCMNotificationListener{

	public function handle(FCMNotification $fcm){
		$fcm->job();
	}
}