<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\Widget;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

use DB,Helper,Str,Cache,Carbon,Input;
use Curl;

class CompactWidgetTransformer extends TransformerAbstract{

	public function transform(Widget $widget){
	     return [
	     	'id' => $widget->id,
	     	'title' => $widget->title,
			'code' => $widget->code,
			'status' => $widget->status,
			'parent_id' => $widget->parent_id,
			'is_parent' => $widget->is_parent ? TRUE : FALSe,
			'type' => $widget->type,
			'display_screen' => $widget->display_screen,
			'short_description' => $widget->short_description,
			'content' => $widget->content,
			'display_type' => $widget->display_type,
	     	'image' => [
				'path' => $widget->directory,
	 			'filename' => $widget->filename,
	 			'path' => $widget->path,
	 			'directory' => $widget->directory,
	 			'full_path' => $widget->path ? "{$widget->directory}/resized/{$widget->filename}" : asset("{$widget->directory}/resized/{$widget->filename}"),
	 			'thumb_path' => $widget->path ? "{$widget->directory}/thumbnails/{$widget->filename}" : asset("{$widget->directory}/thumbnails/{$widget->filename}"),
 			]
	     ];
	}

}