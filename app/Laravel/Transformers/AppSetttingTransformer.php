<?php 

namespace App\Laravel\Transformers;

use App\Laravel\Models\AppSetting;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

class AppSetttingTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'districts',
    ];

	public function transform(AppSetting $setting)
	{
	    return [
	    	'title' => $setting->title,
			'type' => $setting->type,
			'code' => $setting->code,
			'value' => $setting->value,
			'path' => $setting->path,
 			'directory' => $setting->directory,
 			'full_path' => $setting->path ? "{$setting->directory}/resized/{$setting->filename}" : asset("{$setting->directory}/resized/{$setting->filename}"),
 			'thumb_path' => $setting->path ? "{$setting->directory}/thumbnails/{$setting->filename}" : asset("{$setting->directory}/thumbnails/{$setting->filename}"),
	    ];
	}

	public function includeDistricts(AppSetting $setting){
		$collection = Collection::make([
				'I' => ['City Proper','Lapaz'],
				'II' => ['Jaro'],
				'III' => ['Mandurriao','Molo','Arevalo'],
		]);
		return $this->item($collection, new MasterTransformer);
	}
}