<?php 

namespace App\Laravel\Transformers;

use App\Laravel\Models\Establishment;
use App\Laravel\Models\Directory;

use App\Laravel\Transformers\DirectoryTransformer;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

class EstablishmentTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'date','directory'
    ];

	public function transform(Establishment $establishment)
	{
	    return [
	    	'id'			=> $establishment->id,
	    	'name'		=> $establishment->name,
	    	'address'		=> $establishment->address ? : "???",
	    	'contact'		=> $establishment->contact ? : "???",
	    	'geolocation'	=> [
    			"lat"	=> $establishment->geo_long,
    			"long"	=> $establishment->geo_lat,
    		]
	    ];
	}

	public function includeDate(Establishment $establishment){
        $collection = Collection::make([
			'date_db' => $establishment->date_db($establishment->created_at,env("MASTER_DB_DRIVER","mysql")),
			'month_year' => $establishment->month_year($establishment->created_at),
			'time_passed' => $establishment->time_passed($establishment->created_at),
			'timestamp' => $establishment->created_at
    	]);
        return $this->item($collection, new MasterTransformer);
	}

	public function includeDirectory(Establishment $establishment){
        $directory = $establishment->directory ? : new Directory;
        if(!$directory->id) $directory->id = 0;
        return $this->item($directory, new DirectoryTransformer);
	}
	
}