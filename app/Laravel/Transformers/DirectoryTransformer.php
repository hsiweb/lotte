<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\Directory;

use App\Laravel\Transformers\EstablishmentTransformer;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

use DB,Helper,Str,Cache,Carbon,Input;

class DirectoryTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'date','icon','establishments'
    ];

	public function transform(Directory $directory){
	     return [
	     	'id' => $directory->id,
	     	'title' => $directory->title,
	     ];
	}

	public function includeDate(Directory $directory){
        $collection = Collection::make([
			'date_db' => $directory->date_db($directory->created_at,env("MASTER_DB_DRIVER","mysql")),
			'month_year' => $directory->month_year($directory->created_at),
			'time_passed' => $directory->time_passed($directory->created_at),
			'timestamp' => $directory->created_at
    	]);
        return $this->item($collection, new MasterTransformer);
	}

	public function includeIcon(Directory $directory){
        $collection = Collection::make([
			'path' => $directory->path,
 			'directory' => $directory->directory,
 			'full_path' => $directory->path ? "{$directory->directory}/resized/{$directory->filename}" : asset("{$directory->directory}/resized/{$directory->filename}"),
 			'thumb_path' => $directory->path ? "{$directory->directory}/thumbnails/{$directory->filename}" : asset("{$directory->directory}/thumbnails/{$directory->filename}"),
    	]);
        return $this->item($collection, new MasterTransformer);
	}

	public function includeEstablishments(Directory $directory){
		return $this->collection($directory->establishments, new EstablishmentTransformer);
	}
}