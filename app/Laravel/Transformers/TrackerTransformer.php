<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\CRTrackerHeader;
use App\Laravel\Models\CRTracker;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

use DB,Helper,Str,Cache,Carbon,Input;

class TrackerTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'more'
    ];

	public function transform(CRTrackerHeader $header){

		$response = [
			'id' => $header->id,
			'total_duration' => "",
			'schedule_appointment' => Helper::date_format($header->appointment_schedule, "Y-m-d hA"),
		];

		foreach ($header->info()->get() as $process) {
			$response[$process->process_code] = $process->status;
		}

	    return $response;
	}

	public function includeMore(CRTrackerHeader $header){
		$processes = array();
		foreach ($header->info()->get() as $process) {
			array_push($processes, [
				'title' => $process->process_title,
				'code' => $process->process_code,
				'duration' => $process->duration ? ($process->duration . " mins") : "?",
				'status' => $process->status,
				'remarks' => $process->remarks
			]);
		}
        $collection = Collection::make($processes);
        return $this->item($collection, new MasterTransformer);
	}
}