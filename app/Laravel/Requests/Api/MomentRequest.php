<?php 

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;

class MomentRequest extends ApiRequestManager {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{	
		return [
			// 'content' => 'required',
			'file' => "image",
		];
	}

	public function messages(){
		return [
			'required' => "Field is required.",
		];
	}
}