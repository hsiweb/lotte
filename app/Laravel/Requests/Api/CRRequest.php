<?php 

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;

class CRRequest extends ApiRequestManager {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{	
		$rules = [
			'name' => "required",
			'email' => "email",
			// 'phone' => "required",
			// 'mobile' => "required",
			// 'target_table' => "required",
			'type' => "required|valid_type",
			'sub_type' => "required|valid_sub_type",
			'contact_number'	=> "required"
			// 'remarks' => "required",
			// 'status' => "required",
			// 'zone' => "required|in:I,II,III",
		];

		switch ($this->type) {
			case 'assessor':
				$rules += [
					'property_owner' => "required",
					'property_location' => "required",
				];

				// if($this->sub_type == "assessor_with_taxmapping") $rules['pin'] = "required";
			break;

			case 'mac':
				$rules += [
					'appointment_date'	=> "required|date|valid_appointment_schedule",
				];
			break;
		}

		return $rules;
	}

	public function messages(){
		return [
			'required' => "Field is required.",
			'type.valid_type' => "Invalid request type",
			'sub_type.valid_sub_type' => "Invalid request subtype",
			'zone.in' => "Invalid zone",
			'appointment_date.date'	=> "Invalid date.",
			'appointment_date.valid_appointment_schedule' => "No available slot for the chosen appointment schedule."
		];
	}
}