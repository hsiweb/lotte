<?php namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;

class ResetPasswordRequest extends ApiRequestManager {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules(){

		return [
				'email'				=> "required|email",
				'password'			=> "required|confirmed",
				'validation_token'	=> "required",
			];
			

		
	}

	public function messages(){
		return [
			'email'					=> "Invalid email format.",
			'required'				=> "Field is required.",
		];
	}

}
