<?php 

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;
use Input;

class AvatarRequest extends ApiRequestManager {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules(){

		$rules = [
				'file'	=> "required|mimes:jpg,jpeg,png",
			];


		$input = Input::all();

		if(isset($input['file']) AND is_array($input['file'])){
			foreach ($input['file'] as $index => $file) {
				$rules['file.'.$index] = "mimes:jpg,jpeg,png";
			}
		}
		
		return $rules;		
	}

	public function messages(){
		return [
			'required'			=> "Image file not found.",
			'image'				=> "File must be an image.",
			'mimes'				=> "Image format is not valid.",
		];
	}

}