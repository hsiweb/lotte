<?php 

namespace App\Laravel\Requests;

use App\Http\Requests\Request;
use Session;

class RequestManager extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	public function response(array $errors)
	{
		if ($this->ajax() || $this->wantsJson())
		{
			return new JsonResponse($errors, 422);
		}

		Session::flash('notification-status','failed');
		Session::flash('notification-title',"Oops!");
		Session::flash('notification-msg','Some fields are not accepted.');

		return $this->redirector
				->to($this->getRedirectUrl())
                ->withInput($this->except($this->dontFlash))
                ->withErrors($errors, $this->errorBag);
	}
}