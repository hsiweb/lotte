<?php 

namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class AdminRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			'type' => "required",
			'fname' => "required",
			'lname' => "required",
			'email' => "required|unique:user,email,".$id,
			'password' => "required|confirmed",
			'file' => "image",
			// 'zone' => "required_unless:type,super_user|in:none,I,II,III",
		];

		// if(!in_array($this->get('type'), ['super_user','admin'])){
			// $rules['zone'] = "required|in:I,II,III";
		// }

		if($id)	unset($rules['password']);
		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
			// 'zone.required_unless' => "Required only when user type is Administrator",
			'zone.required' => "Required only when user type is Assessor Employee.",
			'zone.in' => "Zone should only be I, II or III when user type is Assessor Employee.",
		];
	}
}