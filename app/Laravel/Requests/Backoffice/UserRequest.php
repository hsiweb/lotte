<?php 

namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class UserRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			'fname' => "required",
			'lname' => "required",
			'email' => "required|unique:user,email,".$id,
			'password' => "required|confirmed",
			'file' => "image",
		];

		if($id)	unset($rules['password']);
		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
		];
	}
}