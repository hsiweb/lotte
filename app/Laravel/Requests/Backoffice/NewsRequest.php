<?php 

namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class NewsRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			'title' => "required",
			'content' => "required|min:150",
			'file' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
			'min' => "This field should contain atleast 150 characters.",
		];
	}
}