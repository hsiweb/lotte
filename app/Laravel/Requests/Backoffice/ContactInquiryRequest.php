<?php 

namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class ContactInquiryRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			'name' => "required",
			'email' => "required|email",
			'contact_no' => "required|numeric",
			'subject' => "required",
			'captcha' => "required|confirmed",
			'captcha_confirmation' => "required",
			'message' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This field is required.",
			'confirmed' => "Invalid captcha confirmation.",
			'numeric' => "Invalid contact number.",
		];
	}
}