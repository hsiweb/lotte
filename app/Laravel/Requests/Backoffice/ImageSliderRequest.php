<?php 

namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class ImageSliderRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			'file' => "required",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
		];
	}
}