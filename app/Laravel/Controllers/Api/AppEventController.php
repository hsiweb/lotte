<?php 

namespace App\Laravel\Controllers\Api;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\AppEvent;


/**
*
* Transformers used for this controller
*/
use App\Laravel\Transformers\AppEventTransformer;
use App\Laravel\Transformers\CompactAppEventTransformer;

/**
*
* Classes used for this controller
*/
use App\Laravel\Transformers\TransformerManager;

use Illuminate\Contracts\Auth\Guard;
use Helper,Carbon,File,URL,Auth,Str,Input,DB,Image,Validator;


class AppEventController extends Controller{

	protected $response;

	public function __construct(Guard $auth){
		$this->auth = $auth;
		$this->response = array(
				"msg" => "Bad Request.",
				"status" => FALSE,
				'status_code' => "UNAUTHORIZED"
			);
		$this->response_code = 404;
		$this->transformer = new TransformerManager;
	}

	public function index($format = "json"){
		try{
			switch(Str::lower($format)){
				case 'json' :

					$now = Carbon::now();
					$month = Input::get('month') ? : $now->month;
					$year = Input::get('year') ? : $now->year;

					try {
						$day = Carbon::createFromDate($year,$month,1,"Asia/Manila");
					} catch (\Exception $e) {
						$day = FALSE;
					}

					$this->response['msg'] = "Invalid date range.";
					$this->response['status_code'] = "INVALID_RANGE";
					$this->response['status'] = FALSE;

					if($day){

						// $events = AppEvent::published()->with('author')
						// 		->where('city_id',Input::get('city_id',0))
						// 		->month($month)->year($year)
						// 		->orderBy('posted_at',"ASC")
						// 		->get();

						$events = AppEvent::select(DB::raw('DATE(posted_at) AS `posted_at`'))->orderBy('posted_at',"ASC")->distinct()->get();

						$month_name = Carbon::createFromFormat('!m', $month)->format('F');
						$this->response['events'] = $this->transformer->transform($events, new CompactAppEventTransformer, 'collection');
						$this->response['msg'] = "Event List of ".ucwords($month_name).", ".$year;
						$this->response['status_code'] = "EVENT_LIST";
						$this->response['status'] = TRUE;
						$this->response_code = 200;
					}

				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 403;
			}

			return response()->json($this->response,$this->response_code);
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function show($format = "json"){
		try{
			switch(Str::lower($format)){
				case 'json' :
					$id = Input::get('event_id',0);
					$event = AppEvent::with('author')->find($id);

					$this->response['msg'] = "Record not found.";
					$this->response['status_code'] = "EVENT_NOT_FOUND";
					$this->response['status'] = FALSE;

					if($event){
						$this->response['event'] = $this->transformer->transform($event, new AppEventTransformer, 'item');
						$this->response['msg'] = "Event Details.";
						$this->response['status_code'] = "EVENT_INFO";
						$this->response['status'] = TRUE;
						$this->response_code = 200;
					}
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
			}

			return response()->json($this->response,$this->response_code);
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function by_date($format = "json"){
		try{
			switch(Str::lower($format)){
				case 'json' :

					$validator = Validator::make(Input::all(), [
			            'date' => 'sometimes|date',
			        ]);

			        if ($validator->fails()) {
			            $this->response['msg'] = "Invalid date input.";
						$this->response['status_code'] = "INVALID_DATE";
						$this->response['status'] = FALSE;
						goto callback;
			        }

			        $day = Helper::date_db(Input::get('date',Carbon::now()));
			        $formatted_date = Helper::date_format($day, "F d, Y");
		        	$events = AppEvent::with('author')->whereRaw("DATE(`posted_at`) = ?", [$day])->get();

					$this->response['events'] = $this->transformer->transform($events, new AppEventTransformer, 'collection');
					$this->response['msg'] = "Event list for {$formatted_date}.";
					$this->response['status_code'] = "EVENT_LIST";
					$this->response['status'] = TRUE;
					$this->response_code = 200;
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
			}

			callback:
			return response()->json($this->response,$this->response_code);
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}


}