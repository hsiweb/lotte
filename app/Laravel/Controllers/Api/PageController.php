<?php

namespace App\Laravel\Controllers\Api;

use App\Laravel\Controllers\Api\Controller;

use App\Laravel\Models\Page;

use App\Laravel\Transformers\TransformerManager;
use App\Laravel\Transformers\PageTransformer;

use Input, Str;


class PageController extends Controller{


	protected $response;

	public function __construct(){
		$this->user_id = Input::get('auth_id',0);

		$this->response = array(
				"msg" => "Bad Request.",
				"status" => FALSE,
				'status_code' => "UNAUTHORIZED"
			);
		$this->response_code = 401;
		$this->transformer = new TransformerManager;
		// $this->cache_expiration = Helper::get_cache_expiry();
	}

	public function index($format = ""){
		try{
			$per_page = Input::get("per_page",10);
			$pages = Page::where('status',"published")->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($pages,new PageTransformer,'collection');
			$this->response['has_morepage'] = $pages->hasMorePages();
			$this->response['msg'] = "List of pages.";
			$this->response['status_code'] = "PAGE_LIST";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function show($format = ""){
		try{
			$code = Input::get('code');
			$page = Page::where('code',$code)->where('status',"published")->first();

			$this->response['data'] = $this->transformer->transform($page, new PageTransformer,'item');
			$this->response['msg'] = "Page Details.";
			$this->response['status_code'] = "PAGE_DETAILS";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

				case 'html' :
					return view('api.webview', ['title' => $page->title, 'content' => $page->content]);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

}