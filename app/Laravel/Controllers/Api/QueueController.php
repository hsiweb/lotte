<?php

namespace App\Laravel\Controllers\Api;

use App\Laravel\Controllers\Api\Controller;

use App\Laravel\Models\Queue;
use App\Laravel\Models\QueueForDisplay;

use App\Laravel\Requests\Api\QueueRequest;

use App\Laravel\Transformers\TransformerManager;
use App\Laravel\Transformers\QueueTransformer;
use App\Laravel\Transformers\QueueForDisplayTransformer;
use App\Laravel\Transformers\MasterTransformer;

use Input, Str, Helper, Carbon,DB;


class QueueController extends Controller{

	protected $response;

	public function __construct(){
		$this->user_id = Input::get('auth_id',0);

		$this->response = array(
				"msg" => "Bad Request.",
				"status" => FALSE,
				'status_code' => "UNAUTHORIZED"
			);
		$this->response_code = 401;
		$this->transformer = new TransformerManager;
	}

	public function index($format = "json") {
		try{

			QueueForDisplay::where('status',"pending")->update(['status' => "completed"]);

			$for_display = Queue::whereIn('status',["for_display","completed","on_going"])
								->whereDate('created_at', "=", Carbon::today()->toDateString())
								->orderBy('teller_id',"DESC")
								->orderBy('sequence_no',"DESC")
								->get();

			$priority_lane = $for_display->where('service_type', "priority_lane")->first();
			$health_service = $for_display->where('service_type', "health_service")->first();
			$scholarship_program = $for_display->where('service_type', "scholarship_program")->first();
			$social_service = $for_display->where('service_type', "social_service")->first();
			$legal_service = $for_display->where('service_type', "legal_service")->first();
			$administrative = $for_display->where('service_type', "administrative")->first();
			$response = [
				'priority_lane' => $priority_lane ? $priority_lane->queue_no : NULL,
				'health_service' => $health_service ? $health_service->queue_no : NULL,
				'scholarship_program' => $scholarship_program ? $scholarship_program->queue_no : NULL,
				'social_service' => $social_service ? $social_service->queue_no : NULL,
				'legal_service' => $legal_service ? $legal_service->queue_no : NULL,
				'administrative' => $administrative ? $administrative->queue_no : NULL,
			];

			$this->response['data'] = $this->transformer->transform($response, new MasterTransformer,'item');
			$this->response['msg'] = "Current Queues.";
			$this->response['status_code'] = "CURRENT_QUEUE_LIST";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function for_display($format = "json") {
		try {

			$for_display = QueueForDisplay::where('status',"pending")->get();

			$this->response['data'] = $this->transformer->transform($for_display, new QueueForDisplayTransformer, 'collection');
			$this->response['msg'] = "For Display Queues.";
			$this->response['status_code'] = "FOR_DISPLAY_QUEUE_LIST";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		} catch (Exception $e) {
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function store($format = "json", QueueRequest $request) {
		try{
			$new_queue = new Queue;
			$new_queue->fill($request->all());
			$new_queue->sequence_no = Helper::next_sequence($request->get('service_type'));
			$new_queue->queue_no = Helper::create_queue_no($request->get('service_type'), $new_queue->sequence_no, $request->get('source',"webapp"));
			$new_queue->is_priority = $request->get('is_priority',"no");
			$new_queue->status = "pending";
			$new_queue->source = $request->get('source',"webapp");

			if($new_queue->save()) {
				$this->response['data'] = $this->transformer->transform($new_queue, new QueueTransformer, 'item');
				$this->response['msg'] = "A new queue has been created.";
				$this->response['status_code'] = "QUEUE_CREATED";
				$this->response['status'] = TRUE;
				$this->response_code = 201;
			}else{
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
				$this->response_code = 507;
			}

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

			}
		}
		catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function flash($format = "json") {
		try {

			$queue_id = Input::get("queue_id", 0);
			$teller_id = Input::get("teller_id", 0);

			$queue = Queue::find($queue_id);

			if(in_array($queue->status, ["on_going","completed","cancelled"]) AND $queue->teller_id) {
				$this->response['msg'] = "Unable to flash, this record is already processed.";
				$this->response['status_code'] = "ALREADY_PROCESSED";
				$this->response['status'] = FALSE;
				$this->response_code = 409;
				goto callback;
			}

			$queue->status = "for_display";
			$queue->teller_id = $teller_id;

			if($queue->save()) {
				$for_display = new QueueForDisplay;
				$for_display->fill(['queue_id' => $queue->id, 'queue_no' => $queue->queue_no, 'service_type' => $queue->service_type, 'status' => "pending", 'is_priority' => $queue->is_priority]);
				$for_display->save();

				$this->response['data'] = $this->transformer->transform($queue, new QueueTransformer, 'item');
				$this->response['msg'] = "A queue has been flashed.";
				$this->response['status_code'] = "QUEUE_FLASHED";
				$this->response['status'] = TRUE;
				$this->response_code = 201;
			}else{
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
				$this->response_code = 507;
			}

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

			}

		} catch (Exception $e) {
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function flash_for_display($format = "json") {
		try {

			$for_display_id = Input::get('for_display_id', 0);
			$for_display = QueueForDisplay::find($for_display_id);

			if($for_display->status != "pending") {
				$this->response['msg'] = "Unable to flash, this record is already processed.";
				$this->response['status_code'] = "ALREADY_PROCESSED";
				$this->response['status'] = FALSE;
				$this->response_code = 409;
				goto callback;
			}

			$for_display->status = "completed";

			if($for_display->save()) {

				$queue = Queue::find($for_display->queue_id);
				$for_display->delete();
				

				if($queue->is_priority == "yes"){
					// $msg = $queue ? (Str::title(str_replace("_", " ", $queue->service_type)) . " is now serving client " . $queue->queue_no) : ("Now serving client " . $queue->queue_no);

					if($queue){
						$msg = "Please proceed to Priority Lane No. $queue->queue_no";
						$txt_speech = "Please proceed to Priority Lane Number $queue->queue_no";
					}

					// $txt_speech = $queue ? (Str::title(str_replace("_", " ,", $queue->service_type)) . " is, now serving, client, " . Helper::syllabus($queue->queue_no)) : ("Now serving, client, " . Helper::syllabus($queue->queue_no));
				}else{
					$msg = $queue ? "Please proceed to ".(Str::title(str_replace("_", " ", $queue->service_type)) . " No. " . $queue->queue_no) : ("Now serving client " . $queue->queue_no);
					// $txt_speech = $queue ? "Please proceed,  to ". (Str::title(str_replace("_", " ,", $queue->service_type)) . " Number. "  . Helper::syllabus($queue->queue_no)) : ("Now serving, client, " . Helper::syllabus($queue->queue_no));
					$txt_speech = $queue ? "Please proceed". " Number " . $queue->queue_no." '{$queue->name}' to ".(Str::title(str_replace("_", " ", $queue->service_type)) ) : ("Now serving client " . $queue->queue_no);
				}
				

				$this->response['data'] = $this->transformer->transform($queue, new QueueTransformer, 'item');
				$this->response['msg'] =  $msg ;
				$this->response['txt_speech'] = $txt_speech;
				$this->response['status_code'] = "QUEUE_FOR_DISPLAY_FLASHED";
				$this->response['status'] = TRUE;
				$this->response_code = 201;
			}else{
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
				$this->response_code = 507;
			}

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

			}

		} catch (Exception $e) {
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function start($format = "json") {
		try {

			$queue_id = Input::get("queue_id", 0);
			$teller_id = Input::get("teller_id", 0);

			$queue = Queue::find($queue_id);

			if(in_array($queue->status, ["on_going","completed","cancelled"]) AND $queue->teller_id) {
				$this->response['msg'] = "Unable to start, this record is already processed.";
				$this->response['status_code'] = "ALREADY_PROCESSED";
				$this->response['status'] = FALSE;
				$this->response_code = 409;
				goto callback;
			}

			$queue->teller_id = $teller_id;
			$queue->status = "on_going";

			if($queue->save()) {
				$this->response['data'] = $this->transformer->transform($queue, new QueueTransformer, 'item');
				$this->response['msg'] = "A queue has been started.";
				$this->response['status_code'] = "QUEUE_STARTED";
				$this->response['status'] = TRUE;
				$this->response_code = 201;
			}else{
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
				$this->response_code = 507;
			}

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

			}

		} catch (Exception $e) {
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function stop($format = "json") {
		try {

			$queue_id = Input::get("queue_id", 0);
			$teller_id = Input::get("teller_id", 0);

			$queue = Queue::find($queue_id);

			if($queue->status != "on_going") {
				$this->response['msg'] = "Unable to stop, queue has not started yet.";
				$this->response['status_code'] = "QUEUE_NOT_STARTED";
				$this->response['status'] = FALSE;
				$this->response_code = 409;
				goto callback;
			}

			if($queue->teller_id != $teller_id) {
				$this->response['msg'] = "Teller is unauthorized to stop this queue.";
				$this->response['status_code'] = "UNAUTHORIZED_ACCESS";
				$this->response['status'] = FALSE;
				$this->response_code = 409;
				goto callback;	
			}

			$queue->teller_id = $teller_id;
			$queue->status = "completed";

			if($queue->save()) {
				$this->response['data'] = $this->transformer->transform($queue, new QueueTransformer, 'item');
				$this->response['msg'] = "A queue has been completed.";
				$this->response['status_code'] = "QUEUE_COMPLETED";
				$this->response['status'] = TRUE;
				$this->response_code = 201;
			}else{
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
				$this->response_code = 507;
			}

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

			}

		} catch (Exception $e) {
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function cancel($format = "json") {
		try {

			$queue_id = Input::get("queue_id", 0);
			$teller_id = Input::get("teller_id", 0);

			$queue = Queue::find($queue_id);

			if(in_array($queue->status, ["on_going","completed","cancelled"]) AND $queue->teller_id) {
				$this->response['msg'] = "Unable to cancel, this record is already processed.";
				$this->response['status_code'] = "ALREADY_PROCESSED";
				$this->response['status'] = FALSE;
				$this->response_code = 409;
				goto callback;
			}

			$queue->teller_id = $teller_id;
			$queue->status = "cancelled";

			if($queue->save()) {
				$this->response['data'] = $this->transformer->transform($queue, new QueueTransformer, 'item');
				$this->response['msg'] = "A queue has been cancelled.";
				$this->response['status_code'] = "QUEUE_CANCELLED";
				$this->response['status'] = TRUE;
				$this->response_code = 201;
			}else{
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
				$this->response_code = 507;
			}

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

			}

		} catch (Exception $e) {
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function prioritize($format = "json") {
		try {

			$queue_id = Input::get("queue_id", 0);
			$teller_id = Input::get("teller_id", 0);

			$queue = Queue::find($queue_id);

			$queue->is_priority = "yes";
			$queue->teller_id = $teller_id;

			if($queue->save()) {
				$this->response['data'] = $this->transformer->transform($queue, new QueueTransformer, 'item');
				$this->response['msg'] = "A queue has been prioritized.";
				$this->response['status_code'] = "QUEUE_PRIORITIZED";
				$this->response['status'] = TRUE;
				$this->response_code = 201;
			}else{
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
				$this->response_code = 507;
			}

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

			}

		} catch (Exception $e) {
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function get_service_data($format = "json"){
		try{
			$service_type = Input::get('_type');
			$excluded_id = explode(",",Input::get('_ids',"0"));
			$current_date = Helper::date_db(Carbon::now());

			if($service_type == "priority_lane") {
				$list = Queue::whereNotIn("id",$excluded_id)->whereRaw("DATE(created_at) = '{$current_date}'")->where('is_priority',"yes")->where(function($query){
					return $query->where('status','pending')
								 ->orWhere('status','for_display')
								 ->orWhere('status','on_going');

					})->orderBy('created_at',"ASC")->get();
			} else {
				$list = Queue::whereNotIn("id",$excluded_id)->whereRaw("DATE(created_at) = '{$current_date}'")->where('service_type',$service_type)->where('is_priority',"no")->where(function($query){
					return $query->where('status','pending')
								 ->orWhere('status','for_display')
								 ->orWhere('status','on_going');

					})->orderBy('created_at',"ASC")->get();	
			}
			

			$this->response['data'] = $this->transformer->transform($list, new QueueTransformer, 'collection');
			$this->response['msg'] = "Incoming queue.";
			$this->response['status_code'] = "INCOMING_QUEUE";
			$this->response['status'] = TRUE;
			$this->response_code = 200;
			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

			}

		} catch (Exception $e) {
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function get_last_data($format = "json"){
		try{
			$current_date = Helper::date_db(Carbon::now());
			$this->response['total_queue'] = Queue::whereRaw("DATE(created_at) = '{$current_date}'")->count();
			$this->response['most_queue'] = Queue::select(DB::raw("COUNT(*) as sum_queue"),"service_type")->whereRaw("DATE(created_at) = '{$current_date}'")->groupBy("service_type")->orderByRaw("sum_queue DESC")->first();

			$recent_queue = Queue::whereRaw("DATE(created_at) = '{$current_date}'")->orderBy('created_at',"DESC")->take(5)->get();
			$this->response['recent_queue'] = $this->transformer->transform($recent_queue, new QueueTransformer, 'collection');
			$this->response['msg'] = "Updated data list.";
			$this->response['status_code'] = "UPDATED_DATA";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

			}

		} catch (Exception $e) {
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

}
