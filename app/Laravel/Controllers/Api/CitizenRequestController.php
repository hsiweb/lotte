<?php 

namespace App\Laravel\Controllers\Api;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\CitizenRequest;
use App\Laravel\Models\AssessorRequestWithTaxmapping;
use App\Laravel\Models\AssessorRequestSimple;
use App\Laravel\Models\MacRequest;
use App\Laravel\Models\Queue;

use App\Laravel\Models\CitizenRequestLog;
use App\Laravel\Models\UserLog;
use App\Laravel\Models\User;
use App\Laravel\Models\Survey;

use App\Laravel\Models\CRModule;
use App\Laravel\Models\CRCategory;
use App\Laravel\Models\CRSubCategory;
use App\Laravel\Models\CRProcess;
use App\Laravel\Models\CRTracker;
use App\Laravel\Models\CRTrackerHeader;

/**
*
* Requests used for this controller
*/
use App\Laravel\Requests\Api\CRRequest;
use App\Laravel\Requests\Api\Request;
use App\Laravel\Requests\Api\SurveyRequest;

/**
*
* Transformers used for this controller
*/
use App\Laravel\Transformers\CitizenRequestTransformer;

/**
*
* Classes used for this controller
*/
use App\Laravel\Transformers\TransformerManager;

use Event;
use App\Laravel\Events\LogCitizenRequest;

use Helper, Carbon, Input, Str, ImageUploader, DB;

class CitizenRequestController extends Controller{

	protected $response;

	public function __construct(){
		$this->user_id = Input::get('auth_id',0);
		$this->response = array(
				"msg" => "Bad Request.",
				"status" => FALSE,
				'status_code' => "UNAUTHORIZED"
			);
		$this->response_code = 401;
		$this->transformer = new TransformerManager;
	}

	public function index($format = "json"){
		try{
			$per_page = Input::get('per_page',10);
			$requests = CitizenRequest::orderBy('created_at',"DESC")->paginate($per_page);
			$this->response['data'] = $this->transformer->transform($requests, new CitizenRequestTransformer, 'collection');
			$this->response['has_morepage'] = $requests->hasMorePages();
			$this->response['msg'] = "Citizen Request List.";
			$this->response['status_code'] = "CITIZEN_REQUEST_LIST";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function my_requests($format = "json"){
		try{
			$per_page = Input::get('per_page',10);
			$category = Input::get('category');
			$requests = CitizenRequest::where('user_id',$this->user_id)->where('category',$category)->orderBy('created_at',"DESC")->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($requests, new CitizenRequestTransformer, 'collection');
			$this->response['has_morepage'] = $requests->hasMorePages();
			$this->response['msg'] = "My Request List.";
			$this->response['status_code'] = "MY_REQUEST_LIST";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function show($format = "json"){
		try{
			$request = CitizenRequest::where('id',Input::get('request_id',0))->first();
			$this->response['data'] = $this->transformer->transform($request, new CitizenRequestTransformer, 'item');
			$this->response['msg'] = "Request Details.";
			$this->response['status_code'] = "REQUEST_INFO";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function store(CRRequest $request, $format = "json"){
		try{	
			$new_request = new CitizenRequest;

			DB::beginTransaction();
			$user = User::select(DB::raw("CONCAT_WS(' ', fname, lname) AS name"), "contact_number", "age", "gender")->find($this->user_id);

			try {

				// Workaround to fetch beneficiary details
				$beneficiary = array();
				foreach($request->except(['api_token','auth_id','appointment_date','type','sub_type']) as $key => $input) {
					$beneficiary["beneficiary_{$key}"] = $input;
				}

				// Fetch request info
				$cr_module = CRModule::where('code', $request->get('module', "mac"))->first();
				$cr_category = CRCategory::where('code', $request->get('category', "social_service"))->first();
				$cr_subcategory = CRSubCategory::where('code', $request->get('subcategory', $request->get('sub_type')))->first();
				$cr_processes = CRProcess::where('cr_subcategory_id', $cr_subcategory->id)->orderBy('sequence',"ASC")->get();

				$appointment_date = Helper::datetime_db($request->get('appointment_date', Carbon::now()));

				// Create new citizen request
				$new_request = new CitizenRequest;
				$new_request->fill($beneficiary + $user->toArray());
				$new_request->user_id = $this->user_id;
				$new_request->status = "pending";
				$new_request->remarks = "Request submitted.";
				$new_request->title = $cr_subcategory->title;
				$new_request->code = Helper::create_request_code($cr_category->id, $appointment_date);
				
				// Just to make sure the existing mobile app wont fail
				$new_request->target_table = $cr_module->code;
				$new_request->type = $cr_category->code;
				$new_request->sub_type = $cr_subcategory->code;

				$new_request->module = $cr_module->code;
				$new_request->category = $cr_category->code;
				$new_request->subcategory = $cr_subcategory->code;

				$new_request->save();

				// Create tracker header
				$header = new CRTrackerHeader;
				$header->citizen_request_id = $new_request->id;
				$header->cr_category_id = $cr_category->id;
				$header->cr_subcategory_id = $cr_subcategory->id;
				$header->category_title = $cr_category->title;
				$header->category_code = $cr_category->code;
				$header->subcategory_title = $cr_subcategory->title;
				$header->subcategory_code = $cr_subcategory->code;
				$header->appointment_schedule = $appointment_date; 
				$header->save();

				// Create copy of processes to tracker table
				foreach ($cr_processes as $index => $process) {
					$tracker = new CRTracker;
					$tracker->cr_tracker_header_id = $header->id;
					$tracker->cr_process_id = $process->id;
					$tracker->process_title = $process->title;
					$tracker->process_code = $process->code;
					if($index == 0) $tracker->from = Carbon::now();
					$tracker->save();
				}

				// Create queue
				$new_queue = new Queue;
				$new_queue->service_type = $cr_category->code;
				$new_queue->sequence_no = Helper::next_sequence($cr_category->code);
				$new_queue->queue_no = Helper::create_queue_no($cr_category->code, $new_queue->sequence_no, "mobile");
				$new_queue->status = "pending";
				$new_queue->source = "mobile";
				$new_queue->save();

				$data = new LogCitizenRequest(['citizen_request_id' => $new_request->id, 'data' => ['user_id' => $this->user_id,'remarks'=>$new_request->remarks]]);
				Event::fire('log-citizen-request', $data);

				$new_user_log = new UserLog;
				$new_user_log->fill(['user_id' => $this->user_id, 'msg' => "A request has been submitted.", 'type' => "MAC", 'sub_type' => $new_request->category, 'reference_id' => $new_request->id, 'title' => "Request submitted."]);
				$new_user_log->save();

				DB::commit();
				$this->response['data'] = $this->transformer->transform($new_request, new CitizenRequestTransformer, 'item');
				$this->response['msg'] = "Your request has been sent";
				$this->response['status_code'] = "REQUEST_SENT";
				$this->response['status'] = TRUE;
				$this->response_code = 201;

			} catch (Exception $e) {
				DB::rollBack();
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
				$this->response_code = 507;
			}
			
			// $new_request->user_id = $this->user_id;
			// $new_request->status = "pending";
			// $new_request->remarks = "Request submitted.";
			// $new_request->code = Helper::service_code(['type' => $request->get('sub_type'),'appointment_date' => $request->get('appointment_date')]);
			
			// $new_request->sub_type = Helper::get_service($request->get('sub_type'));
			// $new_request->title = Helper::get_service_title($request->get('sub_type'));

			// if($new_request->save()) {

			// 	// $new_request->code = "REQ" . str_pad($new_request->id, 8, 0, STR_PAD_LEFT);

			// 	switch ($new_request->type) {
			// 		case 'mac' :

			// 			$new_request->target_table = "mac_request";
			// 			$mac_request = new MacRequest;

			// 			$mac_request->citizen_request_id = $new_request->id;
			// 			$mac_request->schedule_appointment = Helper::date_db($request->get('appointment_date'));

			// 			if($mac_request != NULL) {

			// 				$valid_tasks = ['info_validation','assessment_evaluation','final_interview','requirement_submission'];

			// 				$mac_request->{$valid_tasks[0] . "_from"} =  Carbon::now();
			// 				$mac_request->save();
			// 			}
			// 		break;
			// 	}

			// 	$new_request->save();

			// 	$data = new LogCitizenRequest(['citizen_request_id' => $new_request->id, 'data' => ['user_id' => $this->user_id,'remarks'=>$new_request->remarks]]);
			// 	Event::fire('log-citizen-request', $data);

			// 	$new_user_log = new UserLog;
			// 	$new_user_log->fill(['user_id' => $this->user_id, 'msg' => "A request has been submitted.", 'type' => "MAC", 'reference_id' => $new_request->id, 'title' => "Request submitted."]);
			// 	$new_user_log->save();

			// 	$this->response['data'] = $this->transformer->transform($new_request, new CitizenRequestTransformer, 'item');
			// 	$this->response['msg'] = "Your request has been sent";
			// 	$this->response['status_code'] = "REQUEST_SENT";
			// 	$this->response['status'] = TRUE;
			// 	$this->response_code = 201;
			// }else{
			// 	$this->response['msg'] = "Unable to store information due to server error. Please try again.";
			// 	$this->response['status_code'] = "DB_ERROR";
			// 	$this->response['status'] = FALSE;
			// 	$this->response_code = 507;
			// }

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
			}
		}
		catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function store_v2(Request $request, $format = "json") {
		try {

			DB::beginTransaction();
			$success = true;
			$user = User::select(DB::raw("CONCAT_WS(' ', fname, lname) AS name"), "contact_number", "age", "gender")->find($this->user_id);

			try {

				// Fetch request info
				$cr_module = CRModule::where('code', $request->get('module', "mac"))->first();
				$cr_category = CRCategory::where('code', $request->get('category'))->first();
				$cr_subcategory = CRSubCategory::where('code', $request->get('subcategory'))->first();
				$cr_processes = CRProcess::where('cr_subcategory_id', $cr_subcategory->id)->orderBy('sequence',"ASC")->get();

				// Create new citizen request
				$new_request = new CitizenRequest;
				$new_request->fill($request->all() + $user->toArray());
				$new_request->user_id = $this->user_id;
				$new_request->status = "pending";
				$new_request->remarks = "Request submitted.";
				$new_request->title = $cr_subcategory->title;
				$new_request->code = Helper::create_request_code($cr_category->id, $request->get('appointment_schedule', Carbon::now()));
				$new_request->save();

				// Create tracker header
				$header = new CRTrackerHeader;
				$header->citizen_request_id = $new_request->id;
				$header->cr_category_id = $cr_category->id;
				$header->cr_subcategory_id = $cr_subcategory->id;
				$header->category_title = $cr_category->title;
				$header->category_code = $cr_category->code;
				$header->subcategory_title = $cr_subcategory->title;
				$header->subcategory_code = $cr_subcategory->code;
				$header->appointment_schedule = $request->get('appointment_schedule', Carbon::now()); 
				$header->save();

				// Create copy of processes to tracker table
				foreach ($cr_processes as $index => $process) {
					$tracker = new CRTracker;
					$tracker->cr_tracker_header_id = $header->id;
					$tracker->cr_process_id = $process->id;
					$tracker->process_title = $process->title;
					$tracker->process_code = $process->code;
					if($index == 0) $tracker->from = Carbon::now();
					$tracker->save();
				}

				// Create queue
				$new_queue = new Queue;
				$new_queue->service_type = $cr_category->code;
				$new_queue->sequence_no = Helper::next_sequence($cr_category->code);
				$new_queue->queue_no = Helper::create_queue_no($cr_category->code, $new_queue->sequence_no, "mobile");
				$new_queue->status = "pending";
				$new_queue->source = "mobile";
				$new_queue->save();

				DB::commit();

			} catch (Exception $e) {
				DB::rollBack();
				$success = false;
			}

			if($success) {

				$data = new LogCitizenRequest(['citizen_request_id' => $new_request->id, 'data' => ['user_id' => $this->user_id, 'remarks' => $new_request->remarks]]);
				Event::fire('log-citizen-request', $data);

				$new_user_log = new UserLog;
				$new_user_log->fill(['user_id' => $this->user_id, 'msg' => "A request has been submitted.", 'type' => "MAC", 'reference_id' => $new_request->id, 'title' => "Request submitted."]);
				$new_user_log->save();

				$this->response['data'] = $this->transformer->transform($new_request, new CitizenRequestTransformer, 'item');
				$this->response['msg'] = "Your request has been sent";
				$this->response['status_code'] = "REQUEST_SENT";
				$this->response['status'] = TRUE;
				$this->response_code = 201;

			} else {
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
				$this->response_code = 507;
			}

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
			}

		} catch (Exception $e) {
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function survey(SurveyRequest $request, $format = "json") {
		DB::beginTransaction();
		try {

			// $tracking_number = $request->get('tracking_id');
			// $citizen_request = CitizenRequest::where('tracking_number',$tracking_number)->first();
			$citizen_request_id = $request->get('citizen_request_id');
			$citizen_request = CitizenRequest::where('id',$citizen_request_id)->first();

			if(!$citizen_request) {
				$this->response['msg'] = "Tracking number is not valid.";
				$this->response['status_code'] = "INVALID_TRACKING_NUMBER";
				$this->response['status'] = FALSE;
				$this->response_code = 404;
				goto callback;
			}

			if($citizen_request->status != "done") {
				$this->response['msg'] = "This request is not yet completed, unable to submit your survey.";
				$this->response['status_code'] = "CITIZEN_REQUEST_NOT_COMPLETED";
				$this->response['status'] = FALSE;
				$this->response_code = 404;
				goto callback;
			}
			
			$survey_exist = Survey::where('user_id', $this->user_id)->where('citizen_request_id', $citizen_request->id)->first();

			if($survey_exist) {
				$this->response['msg'] = "You have already submitted a survey for this request.";
				$this->response['status_code'] = "SURVEY_ALREADY_SUBMITTED";
				$this->response['status'] = FALSE;
				$this->response_code = 409;
				goto callback;
			}

			$answers = explode("|", $request->get('answers',""));
			foreach ( range(0,13) as $index ) {
				$new_survey = new Survey;
				$new_survey->user_id = $this->user_id;
				$new_survey->citizen_request_id = $citizen_request->id;
				// $new_survey->tracking_number = $citizen_request->tracking_number;
				$new_survey->item_no = $index + 1;
				if(array_key_exists($index, $answers)) {
					$new_survey->answer = $answers[$index];
					$new_survey->equivalent_value = $this->__evaluate_answer($answers[$index]);
				}
				$new_survey->save();
			}

			DB::commit();

			$this->response['msg'] = "Your survey has been sent, thank you.";
			$this->response['status_code'] = "SURVEY_SENT";
			$this->response['status'] = TRUE;
			$this->response_code = 201;

			callback: 

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

			}

		} catch (Exception $e) {
			DB::rollBack();
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	private function __evaluate_answer($answer) {
		switch ($answer) {

			case 'strongly_agree': 
				return 4;
			break;

			case 'strongly_disagree': 
				return 3;
			break;

			case 'somewhat_agree': 
				return 2;
			break;

			case 'somewhat_disagree': 
				return 1;
			break;

			default: return 0;
		}
	}

}