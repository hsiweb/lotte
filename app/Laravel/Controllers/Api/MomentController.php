<?php 

namespace App\Laravel\Controllers\Api;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Moment;

/**
*
* Requests used for this controller
*/
use App\Laravel\Requests\Api\MomentRequest;

/**
*
* Transformers used for this controller
*/
use App\Laravel\Transformers\MomentTransformer;

/**
*
* Classes used for this controller
*/
use App\Laravel\Transformers\TransformerManager;
use Helper, Carbon, Input, Str, ImageUploader;
use Request, GeoIp;

class MomentController extends Controller{

	protected $response;

	public function __construct(){
		$this->user_id = Input::get('auth_id',0);
		$this->response = array(
				"msg" => "Bad Request.",
				"status" => FALSE,
				'status_code' => "UNAUTHORIZED"
			);
		$this->response_code = 401;
		$this->transformer = new TransformerManager;
	}

	public function index($format = "json"){
		try{
			$per_page = Input::get('per_page',10);
			$moments = Moment::where('status','<>',"pending")
					->with('author')->orderBy('created_at',"DESC")
					->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($moments, new MomentTransformer, 'collection');
			$this->response['has_morepage'] = $moments->hasMorePages();
			$this->response['msg'] = "Moment List.";
			$this->response['status_code'] = "MOMENT_LIST";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function my_moments($format = "json"){
		try{
			$per_page = Input::get('per_page',10);
			$moments = Moment::with('author')
						->where('user_id',$this->user_id)
						->orderBy('created_at',"DESC")->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($moments, new MomentTransformer, 'collection');
			$this->response['has_morepage'] = $moments->hasMorePages();
			$this->response['msg'] = "My Moment List.";
			$this->response['status_code'] = "MY_MOMENT_LIST";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function show($format = "json"){
		try{
			$moment = Moment::with('author')
						->where('id',Input::get('moment_id',0))
						->first();

			$this->response['data'] = $this->transformer->transform($moment, new MomentTransformer, 'item');
			$this->response['msg'] = "Moment  Details.";
			$this->response['status_code'] = "MOMENT_DETIALS";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function store(MomentRequest $request, $format = "json"){
		try{	
			$new_moment = new Moment;
			$new_moment->fill($request->all());

			$new_moment->user_id = $this->user_id;

			if($request->hasFile('file')) $new_moment->fill(ImageUploader::upload($request, "uploads/moments", "file"));

			if($new_moment->save()) {
				$this->response['data'] = $this->transformer->transform($new_moment, new MomentTransformer, 'item');
				$this->response['msg'] = "Your moment has been sent";
				$this->response['status_code'] = "MOMENT_SENT";
				$this->response['status'] = TRUE;
				$this->response_code = 201;
			}else{
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
				$this->response_code = 507;
			}

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
			}
		}
		catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function update($format = "json"){
		try{

			// $moment = new Moment;
			$auth_id = Input::get('auth_id');
			$moment_id = Input::get('moment_id');
			$moment = Moment::where('user_id',$auth_id)->where('id',$moment_id)->first();
			// dd(4);
			if(!$moment){
				$this->response['msg'] = "Unable to proceed process. Record not found.";
				$this->response['status_code'] = "NOT_FOUND";
				$this->response['status'] = FALSE;
				$this->response_code = 404;
				goto callback;
			}	

			$moment->fill(Input::only('content'));
			if($moment->save()) {
				$this->response['data'] = $this->transformer->transform($moment, new MomentTransformer, 'item');
				$this->response['msg'] = "Moment successfully update.";
				$this->response['status_code'] = "MOMENT_UPDATE";
				$this->response['status'] = TRUE;
				$this->response_code = 200;
			}else{
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
				$this->response_code = 507;
			}
			callback:
			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
			}

		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}


	public function destroy($format = "json"){
		try{
			$moment = Moment::with('author')
						->where('id',Input::get('moment_id',0))
						->first();

			$moment->delete();

			$this->response['data'] = $this->transformer->transform($moment, new MomentTransformer, 'item');
			$this->response['msg'] = "Moment successfully deleted.";
			$this->response['status_code'] = "MOMENT_DELETED";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

}