<?php

namespace App\Laravel\Controllers\Api;

use App\Laravel\Controllers\Api\Controller;

use App\Laravel\Models\AppSetting;
use App\Laravel\Models\User;

use App\Laravel\Transformers\TransformerManager;
use App\Laravel\Transformers\AppSetttingTransformer;
use App\Laravel\Transformers\UserTransformer;

use App\Laravel\Transformers\MasterTransformer;
use Illuminate\Support\Collection;
use Input, Str;


class AppSettingController extends Controller{


	protected $response;

	public function __construct(){
		$this->user_id = Input::get('auth_id',0);

		$this->response = array(
				"msg" => "Bad Request.",
				"status" => FALSE,
				'status_code' => "UNAUTHORIZED"
			);
		$this->response_code = 401;
		$this->transformer = new TransformerManager;
		// $this->cache_expiration = Helper::get_cache_expiry();
	}

	public function index($format = ""){
		try{
			$settings = AppSetting::where('status',"published")->get();

			$collection = new Collection;
			foreach ($settings as $index => $setting) {
				$collection->put($setting->code, $setting->value);
			}

			$district_collection = Collection::make([
				'data' => [
					'I' => ['City Proper','Lapaz'],
					'II' => ['Jaro'],
					'III' => ['Mandurriao','Molo','Arevalo'],
				]
			]);

			$collection->put('districts',$district_collection);

			$this->response['data'] = $this->transformer->transform($collection, new MasterTransformer,'item');
			$this->response['msg'] = "List of app settings.";
			$this->response['status_code'] = "APP_SETTINGS";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			// $user_id = Input::get('user_id',0);
			// if($user_id) {
			// 	$user = User::find($user_id);
			// 	$this->response['user'] = [ 'data' => $this->transformer->transform($user, new UserTransformer,'item') ];
			// }

			// $this->response['gallery'] = [
			// 	'data' => [
			// 		['image' => "http://lorempixel.com/400/200", 'text' => "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable."],
			// 		['image' => "http://lorempixel.com/400/200", 'text' => "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable."],
			// 		['image' => "http://lorempixel.com/400/200", 'text' => "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable."],
			// 		['image' => "http://lorempixel.com/400/200", 'text' => "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable."],
			// 		['image' => "http://lorempixel.com/400/200", 'text' => "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable."],
			// 	]
			// ];

			// $this->response['districts'] = [
			// 	'I' => ['City Proper','Lapaz'],
			// 	'II' => ['Jaro'],
			// 	'III' => ['Mandurriao','Molo','Arevalo'],
			// ];

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

}