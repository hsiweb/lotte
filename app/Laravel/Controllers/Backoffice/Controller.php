<?php 

namespace App\Laravel\Controllers\Backoffice;

use Illuminate\Support\Collection;
use App\Laravel\Controllers\Controller as MainController;
use Auth, Session;

class Controller extends MainController{

	protected $data;

	public function __construct(){
		self::set_backoffice_routes();
		self::set_user_info();
	}

	public function set_backoffice_routes(){
		$this->data['routes'] = array(
			"dashboard" => ['backoffice.dashboard'],
			'header' => ['backoffice.header.index','backoffice.header.create','backoffice.header.edit','backoffice.header.destroy','backoffice.header.deletes'],
			'image_slider' => ['backoffice.image_slider.index','backoffice.image_slider.create','backoffice.image_slider.edit','backoffice.image_slider.destroy','backoffice.image_slider.deletes'],
			'product' => ['backoffice.product.index','backoffice.product.create','backoffice.product.edit','backoffice.product.destroy','backoffice.product.deletes'],
			'contact' => ['backoffice.contact.index'],
			'news' => ['backoffice.news.index','backoffice.news.create','backoffice.news.edit','backoffice.news.destroy','backoffice.news.deletes'],
			'career' => ['backoffice.career.index','backoffice.career.create','backoffice.career.edit','backoffice.career.destroy','backoffice.career.deletes'],
			'tagline' => ['backoffice.tagline.index','backoffice.tagline.create','backoffice.tagline.edit','backoffice.tagline.destroy','backoffice.tagline.deletes'],
		);
	}

	public function set_user_info(){
		$this->data['auth'] = Auth::user();
	}
	
	public function get_data(){
		return $this->data;
	}
}