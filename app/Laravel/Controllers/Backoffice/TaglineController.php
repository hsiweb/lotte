<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\tagline;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\TaglineRequest;
use App\Laravel\Requests\Backoffice\EditTaglineRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, Carbon, Session, Str, DB,File,Image,ImageUploader;

class TaglineController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['types'] = [ '' => "Choose type", 'mission' => "Mission", 'vision' => "Vision",'trust' => "Trust",'respect' => "Respect",'enthusiasm' => "Enthusiasm",'accountability' => "Accountability",'excellence' => "Excellence",'challenge' => "Challenge"];
	}

	public function index () {
		$this->data['tagline'] = Tagline::orderBy('updated_at',"DESC")->get();
		return view('backoffice.tagline.index',$this->data);
	}

	public function create () {
		return view('backoffice.tagline.create',$this->data);
	}

	public function store (TaglineRequest $request) {
		try {
			$new_tagline = new Tagline;
			$new_tagline->fill($request->all());
			if($request->hasFile('file')){
				$upload = $this->__upload($request);
				$new_tagline->directory = $upload["directory"];
				$new_tagline->filename = $upload["filename"];
			}

			if($new_tagline->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New tagline has been added.");
				return redirect()->route('backoffice.tagline.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$tagline = Tagline::find($id);

		if (!$tagline) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.tagline.index');
		}

		$this->data['tagline'] = $tagline;
		return view('backoffice.tagline.edit',$this->data);
	}

	public function update (EditTaglineRequest $request, $id = NULL) {
		try {
			$tagline = Tagline::find($id);

			if (!$tagline) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.tagline.index');
			}

			$tagline->fill($request->all());
			if($request->hasFile('file')) $tagline->fill(ImageUploader::upload($request, 'uploads/tagline',"file"));

			if($tagline->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A tagline has been updated.");
				return redirect()->route('backoffice.tagline.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$tagline = Tagline::find($id);

			if (!$tagline) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.tagline.index');
			}

			if (in_array($tagline->code, ['version_name','major_version','minor_version','changelogs'])) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Permission denied. This record cannot be deleted.");
				return redirect()->route('backoffice.tagline.index');
			}

			if($tagline->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A tagline has been deleted.");
				return redirect()->route('backoffice.tagline.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function deleted (){
		$this->data['tagline'] = Tagline::onlyTrashed('deleted_at')->orderBy('deleted_at',"DESC")->get();
		return view('backoffice.tagline.deleted',$this->data);
	}

	private function __upload($request, $directory = "uploads/tagline"){
		$file = $request->file("file");
		$ext = $file->getClientOriginalExtension();

		$path_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd");
		$resized_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/resized";
		$thumb_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/thumbnails";

		if (!File::exists($path_directory)){
			File::makeDirectory($path_directory, $mode = 0777, true, true);
		}

		if (!File::exists($resized_directory)){
			File::makeDirectory($resized_directory, $mode = 0777, true, true);
		}

		if (!File::exists($thumb_directory)){
			File::makeDirectory($thumb_directory, $mode = 0777, true, true);
		}

		$filename = Helper::create_filename($ext);

		$file->move($path_directory, $filename); 
		Image::make("{$path_directory}/{$filename}")->fit(1300,500)->save("{$resized_directory}/{$filename}",100);
		Image::make("{$path_directory}/{$filename}")->crop(250,250)->save("{$thumb_directory}/{$filename}",100);

		return [ "directory" => $path_directory, "filename" => $filename ];
	}
}