<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Header;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\HeaderRequest;
use App\Laravel\Requests\Backoffice\EditHeaderRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, headerUploader, Carbon, Session, Str, DB,File,Image,ImageUploader;

class HeaderController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['types'] = [ '' => "Choose type", 'home' => "Home", 'about-history' => "About-History",'about-mission-vision' => "About-Mission Vision",'about-core-values' => "About-Core Values",'products' => "Products",'careers' => "Careers",'about-history' => "About-History"];
		$this->data['statuses'] = [ '' => "Choose status", 'draft' => "Draft", 'published' => "Published"];
	}

	public function index () {
		$this->data['header'] = Header::orderBy('updated_at',"DESC")->get();
		return view('backoffice.header.index',$this->data);
	}

	public function create () {
		return view('backoffice.header.create',$this->data);
	}

	public function store (HeaderRequest $request) {
		try {
			$new_header = new Header;
			$new_header->fill($request->all());
			if($request->hasFile('file')){
				$upload = $this->__upload($request);
				$new_header->directory = $upload["directory"];
				$new_header->filename = $upload["filename"];
			}

			if($new_header->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New header has been added.");
				return redirect()->route('backoffice.header.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$header = Header::find($id);

		if (!$header) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.header.index');
		}

		$this->data['header'] = $header;
		return view('backoffice.header.edit',$this->data);
	}

	public function update (EditHeaderRequest $request, $id = NULL) {
		try {
			$header = Header::find($id);

			if (!$header) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.header.index');
			}

			$header->fill($request->all());
			if($request->hasFile('file')) $header->fill(ImageUploader::upload($request, 'uploads/header',"file"));

			if($header->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A header has been updated.");
				return redirect()->route('backoffice.header.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$header = Header::find($id);

			if (!$header) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.header.index');
			}

			if (in_array($header->code, ['version_name','major_version','minor_version','changelogs'])) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Permission denied. This record cannot be deleted.");
				return redirect()->route('backoffice.header.index');
			}

			if($header->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A header has been deleted.");
				return redirect()->route('backoffice.header.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function deleted (){
		$this->data['header'] = Header::onlyTrashed('deleted_at')->orderBy('deleted_at',"DESC")->get();
		return view('backoffice.header.deleted',$this->data);
	}

	private function __upload($request, $directory = "uploads/header"){
		$file = $request->file("file");
		$ext = $file->getClientOriginalExtension();

		$path_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd");
		$resized_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/resized";
		$thumb_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/thumbnails";

		if (!File::exists($path_directory)){
			File::makeDirectory($path_directory, $mode = 0777, true, true);
		}

		if (!File::exists($resized_directory)){
			File::makeDirectory($resized_directory, $mode = 0777, true, true);
		}

		if (!File::exists($thumb_directory)){
			File::makeDirectory($thumb_directory, $mode = 0777, true, true);
		}

		$filename = Helper::create_filename($ext);

		$file->move($path_directory, $filename); 
		Image::make("{$path_directory}/{$filename}")->fit(1300,500)->save("{$resized_directory}/{$filename}",100);
		Image::make("{$path_directory}/{$filename}")->crop(250,250)->save("{$thumb_directory}/{$filename}",100);

		return [ "directory" => $path_directory, "filename" => $filename ];
	}
}