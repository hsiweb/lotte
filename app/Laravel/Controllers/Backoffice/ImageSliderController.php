<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\ImageSlider;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\ImageSliderRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, ImageSliderUploader, Carbon, Session, Str, DB,File,Image,ImageUploader;

class ImageSliderController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

	public function index () {
		$this->data['image_slider'] = ImageSlider::orderBy('updated_at',"DESC")->get();
		return view('backoffice.image_slider.index',$this->data);
	}

	public function create () {
		return view('backoffice.image_slider.create',$this->data);
	}

	public function store (ImageSliderRequest $request) {
		try {
			$new_image_slider = new ImageSlider;
			$new_image_slider->fill($request->all());
			if($request->hasFile('file')){
				$upload = $this->__upload($request);
				$new_image_slider->directory = $upload["directory"];
				$new_image_slider->filename = $upload["filename"];
			}

			if($new_image_slider->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New ImageSlider has been added.");
				return redirect()->route('backoffice.image_slider.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$image_slider = ImageSlider::find($id);

		if (!$image_slider) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.image_slider.index');
		}

		$this->data['image_slider'] = $image_slider;
		return view('backoffice.image_slider.edit',$this->data);
	}

	public function update (ImageSliderRequest $request, $id = NULL) {
		try {
			$image_slider = ImageSlider::find($id);

			if (!$image_slider) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.image_slider.index');
			}

			$image_slider->fill($request->all());
			if($request->hasFile('file')) $image_slider->fill(ImageUploader::upload($request, 'uploads/image_slider',"file"));

			if($image_slider->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A ImageSlider has been updated.");
				return redirect()->route('backoffice.image_slider.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$image_slider = ImageSlider::find($id);

			if (!$image_slider) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.image_slider.index');
			}

			if (in_array($image_slider->code, ['version_name','major_version','minor_version','changelogs'])) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Permission denied. This record cannot be deleted.");
				return redirect()->route('backoffice.image_slider.index');
			}

			if($image_slider->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A ImageSlider has been deleted.");
				return redirect()->route('backoffice.image_slider.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
	// public function destroy ($id = NULL) {
	// 	try {
	// 		$image = ImageSlider::find($id);

	// 		if (!$image) {
	// 			Session::flash('notification-status',"failed");
	// 			Session::flash('notification-msg',"Record not found.");
	// 			return redirect()->route('backoffice.image_slider.index');
	// 		}

	// 		if (File::exists("{$image->directory}/{$image->filename}")){
	// 			File::delete("{$image->directory}/{$image->filename}");
	// 		}
	// 		if (File::exists("{$image->directory}/resized/{$image->filename}")){
	// 			File::delete("{$image->directory}/resized/{$image->filename}");
	// 		}
	// 		if (File::exists("{$image->directory}/thumbnails/{$image->filename}")){
	// 			File::delete("{$image->directory}/thumbnails/{$image->filename}");
	// 		}
			
	// 		if($image->delete()) {
	// 			Session::flash('notification-status','success');
	// 			Session::flash('notification-msg',"An image has been deleted.");
	// 			return redirect()->route('backoffice.image_slider.index');
	// 		}

	// 		Session::flash('notification-status','failed');
	// 		Session::flash('notification-msg','Something went wrong.');

	// 	} catch (Exception $e) {
	// 		Session::flash('notification-status','failed');
	// 		Session::flash('notification-msg',$e->getMessage());
	// 		return redirect()->back();
	// 	}
	// }

	public function deleted (){
		$this->data['image_slider'] = ImageSlider::onlyTrashed('deleted_at')->orderBy('deleted_at',"DESC")->get();
		return view('backoffice.image_slider.deleted',$this->data);
	}

	private function __upload($request, $directory = "uploads/ImageSlider"){
		$file = $request->file("file");
		$ext = $file->getClientOriginalExtension();

		$path_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd");
		$resized_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/resized";
		$thumb_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/thumbnails";

		if (!File::exists($path_directory)){
			File::makeDirectory($path_directory, $mode = 0777, true, true);
		}

		if (!File::exists($resized_directory)){
			File::makeDirectory($resized_directory, $mode = 0777, true, true);
		}

		if (!File::exists($thumb_directory)){
			File::makeDirectory($thumb_directory, $mode = 0777, true, true);
		}

		$filename = Helper::create_filename($ext);

		$file->move($path_directory, $filename); 
		Image::make("{$path_directory}/{$filename}")->fit(1300,500)->save("{$resized_directory}/{$filename}",100);
		Image::make("{$path_directory}/{$filename}")->crop(250,250)->save("{$thumb_directory}/{$filename}",100);

		return [ "directory" => $path_directory, "filename" => $filename ];
	}
}