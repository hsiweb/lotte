<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\News;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\NewsRequest;
use App\Laravel\Requests\Backoffice\EditNewsRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, Carbon, Session, Str, DB,File,Image,ImageUploader;

class NewsController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

	public function index () {
		$this->data['news'] = News::orderBy('updated_at',"DESC")->get();
		return view('backoffice.news.index',$this->data);
	}

	public function create () {
		return view('backoffice.news.create',$this->data);
	}

	public function store (NewsRequest $request) {
		try {
			$new_news = new News;
			$new_news->fill($request->all());
			if($request->hasFile('file')){
				$upload = $this->__upload($request);
				$new_news->directory = $upload["directory"];
				$new_news->filename = $upload["filename"];
			}

			if($new_news->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New news has been added.");
				return redirect()->route('backoffice.news.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$news = News::find($id);

		if (!$news) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.news.index');
		}

		$this->data['news'] = $news;
		return view('backoffice.news.edit',$this->data);
	}

	public function update (EditNewsRequest $request, $id = NULL) {
		try {
			$news = News::find($id);

			if (!$news) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.news.index');
			}

			$news->fill($request->all());
			if($request->hasFile('file')) $news->fill(ImageUploader::upload($request, 'uploads/news',"file"));

			if($news->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A news has been updated.");
				return redirect()->route('backoffice.news.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$news = News::find($id);

			if (!$news) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.news.index');
			}

			if (in_array($news->code, ['version_name','major_version','minor_version','changelogs'])) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Permission denied. This record cannot be deleted.");
				return redirect()->route('backoffice.news.index');
			}

			if($news->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A news has been deleted.");
				return redirect()->route('backoffice.news.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function deleted (){
		$this->data['news'] = News::onlyTrashed('deleted_at')->orderBy('deleted_at',"DESC")->get();
		return view('backoffice.news.deleted',$this->data);
	}

	private function __upload($request, $directory = "uploads/news"){
		$file = $request->file("file");
		$ext = $file->getClientOriginalExtension();

		$path_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd");
		$resized_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/resized";
		$thumb_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/thumbnails";

		if (!File::exists($path_directory)){
			File::makeDirectory($path_directory, $mode = 0777, true, true);
		}

		if (!File::exists($resized_directory)){
			File::makeDirectory($resized_directory, $mode = 0777, true, true);
		}

		if (!File::exists($thumb_directory)){
			File::makeDirectory($thumb_directory, $mode = 0777, true, true);
		}

		$filename = Helper::create_filename($ext);

		$file->move($path_directory, $filename); 
		Image::make("{$path_directory}/{$filename}")->fit(1300,500)->save("{$resized_directory}/{$filename}",100);
		Image::make("{$path_directory}/{$filename}")->crop(250,250)->save("{$thumb_directory}/{$filename}",100);

		return [ "directory" => $path_directory, "filename" => $filename ];
	}
}