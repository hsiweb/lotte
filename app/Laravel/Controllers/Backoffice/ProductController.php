<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Product;
/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\ProductRequest;
use App\Laravel\Requests\Backoffice\EditProductRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, Carbon, Session, Str, DB,File,Image,ImageUploader;

class ProductController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['biscuits'] = [ '' => "Choose category", 'pepero' => "Pepero", 'toppo' => "Toppo", 'koalas-march' => "Koala's March"];
		$this->data['cakes'] = [ '' => "Choose category", 'custard-cake' => "Custard Cake", 'choco-pie' => "Choco Pie"];
		$this->data['gums'] = [ '' => "Choose category", 'xylitol' => "Xylitol", 'fusen-no-mi' => "Fusen No Mi"];
	}

	public function index () {
		$this->data['product'] = Product::orderBy('updated_at',"DESC")->get();
		return view('backoffice.product.index',$this->data);
	}

	public function create ($category) {
		$this->data['category']=$category;
		return view('backoffice.product.create',$this->data);
	}

	public function store (ProductRequest $request) {
		try {
			$new_product = new Product;
			$new_product->fill($request->all());
			if($request->hasFile('file')){
				$upload = $this->__upload($request);
				$new_product->directory = $upload["directory"];
				$new_product->filename = $upload["filename"];
			}

			if($new_product->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New Product has been added.");
				return redirect()->route('backoffice.product.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$product = Product::find($id);
		$products = Product::where('id',$id)->get();
		foreach($products as $index=>$info){
			$category = $info->category_name;
		}
		if (!$product) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.product.index');
		}

		$this->data['product'] = $product;
		$this->data['category'] = $category;
		return view('backoffice.product.edit',$this->data);
	}

	public function update (EditProductRequest $request, $id = NULL) {
		try {
			$product = Product::find($id);

			if (!$product) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.product.index');
			}

			$product->fill($request->all());
			if($request->hasFile('file')) $product->fill(ImageUploader::upload($request, 'uploads/product',"file"));

			if($product->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A Product has been updated.");
				return redirect()->route('backoffice.product.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$product = Product::find($id);

			if (!$product) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.product.index');
			}

			if (in_array($product->code, ['version_name','major_version','minor_version','changelogs'])) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Permission denied. This record cannot be deleted.");
				return redirect()->route('backoffice.product.index');
			}

			if($product->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A Product has been deleted.");
				return redirect()->route('backoffice.product.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function deleted (){
		$this->data['product'] = Product::onlyTrashed('deleted_at')->orderBy('deleted_at',"DESC")->get();
		return view('backoffice.product.deleted',$this->data);
	}

	private function __upload($request, $directory = "uploads/Product"){
		$file = $request->file("file");
		$ext = $file->getClientOriginalExtension();

		$path_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd");
		$resized_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/resized";
		$thumb_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/thumbnails";

		if (!File::exists($path_directory)){
			File::makeDirectory($path_directory, $mode = 0777, true, true);
		}

		if (!File::exists($resized_directory)){
			File::makeDirectory($resized_directory, $mode = 0777, true, true);
		}

		if (!File::exists($thumb_directory)){
			File::makeDirectory($thumb_directory, $mode = 0777, true, true);
		}

		$filename = Helper::create_filename($ext);

		$file->move($path_directory, $filename); 
		Image::make("{$path_directory}/{$filename}")->fit(1300,500)->save("{$resized_directory}/{$filename}",100);
		Image::make("{$path_directory}/{$filename}")->crop(250,250)->save("{$thumb_directory}/{$filename}",100);

		return [ "directory" => $path_directory, "filename" => $filename ];
	}
}