<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\ContactInquiry;
/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\ContactInquiryRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, Carbon, Session, Str, DB,File,Image,ImageUploader;

class ContactInquiryController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

	public function index () {
		$this->data['contact'] = ContactInquiry::orderBy('updated_at',"DESC")->get();
		return view('backoffice.contact.index',$this->data);
	}
}