<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Career;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\CareerRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, Carbon, Session, Str, DB,File,Image,ImageUploader;

class CareerController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

	public function index () {
		$this->data['career'] = Career::orderBy('updated_at',"DESC")->get();
		return view('backoffice.career.index',$this->data);
	}

	public function create () {
		return view('backoffice.career.create',$this->data);
	}

	public function store (CareerRequest $request) {
		try {
			$new_career = new Career;
			$new_career->fill($request->all());
			if($request->hasFile('file')){
				$upload = $this->__upload($request);
				$new_career->directory = $upload["directory"];
				$new_career->filename = $upload["filename"];
			}

			if($new_career->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"New career has been added.");
				return redirect()->route('backoffice.career.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$career = Career::find($id);

		if (!$career) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.career.index');
		}

		$this->data['career'] = $career;
		return view('backoffice.career.edit',$this->data);
	}

	public function update (CareerRequest $request, $id = NULL) {
		try {
			$career = Career::find($id);

			if (!$career) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.career.index');
			}

			$career->fill($request->all());
			if($request->hasFile('file')) $career->fill(ImageUploader::upload($request, 'uploads/career',"file"));

			if($career->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A career has been updated.");
				return redirect()->route('backoffice.career.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$career = Career::find($id);

			if (!$career) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.career.index');
			}

			if (in_array($career->code, ['version_name','major_version','minor_version','changelogs'])) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Permission denied. This record cannot be deleted.");
				return redirect()->route('backoffice.career.index');
			}

			if($career->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A career has been deleted.");
				return redirect()->route('backoffice.career.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function deleted (){
		$this->data['career'] = Career::onlyTrashed('deleted_at')->orderBy('deleted_at',"DESC")->get();
		return view('backoffice.career.deleted',$this->data);
	}

	private function __upload($request, $directory = "uploads/career"){
		$file = $request->file("file");
		$ext = $file->getClientOriginalExtension();

		$path_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd");
		$resized_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/resized";
		$thumb_directory = $directory."/".Helper::date_format(Carbon::now(),"Ymd")."/thumbnails";

		if (!File::exists($path_directory)){
			File::makeDirectory($path_directory, $mode = 0777, true, true);
		}

		if (!File::exists($resized_directory)){
			File::makeDirectory($resized_directory, $mode = 0777, true, true);
		}

		if (!File::exists($thumb_directory)){
			File::makeDirectory($thumb_directory, $mode = 0777, true, true);
		}

		$filename = Helper::create_filename($ext);

		$file->move($path_directory, $filename); 
		Image::make("{$path_directory}/{$filename}")->fit(1300,500)->save("{$resized_directory}/{$filename}",100);
		Image::make("{$path_directory}/{$filename}")->crop(250,250)->save("{$thumb_directory}/{$filename}",100);

		return [ "directory" => $path_directory, "filename" => $filename ];
	}
}