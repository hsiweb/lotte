<?php 

namespace App\Laravel\Controllers\Frontend;

/*
*
* Models used for this controller
*/
use App\Laravel\Models\User;
use App\Laravel\Models\Header;
use App\Laravel\Models\ImageSlider;
use App\Laravel\Models\Product;
use App\Laravel\Models\News;
use App\Laravel\Models\Career;
use App\Laravel\Models\Tagline;

/*
*
* Requests used for validating inputs
*/


/*
*
* Classes used for this controller
*/
use Helper, Carbon, Session, Str, DB, Curl, Input;

class HomeController extends Controller{

	/*
	*
	* @var Array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['pepero_counter'] = 0;
		$this->data['toppo_counter'] = 0;
		$this->data['koala_counter'] = 0;
		$this->data['custard_cake_counter'] = 0;
		$this->data['choco_pie_counter'] = 0;
		$this->data['xylitol_counter'] = 0;
		$this->data['fusen_no_mi_counter'] = 0;
	}

	public function index() {
		$this->data['header'] = Header::where('type','home')->get();
		return view('frontend.index',$this->data);
	}

	public function about() {
		$this->data['about_history'] = Header::where('type','about-history')->get();
		$this->data['about_mission_vision'] = Header::where('type','about-mission-vision')->get();
		$this->data['about_core_values'] = Header::where('type','about-core-values')->get();
		$this->data['tagline'] = Tagline::all();
		$this->data['image_slider'] = ImageSlider::all();
		return view('frontend.about',$this->data);
	}

	public function contact() {
		return view('frontend.contact',$this->data);
	}

	public function careers() {
		$this->data['careers'] = Header::where('type','careers')->get();
		$this->data['career'] = Career::orderBy('updated_at',"DESC")->get()->toArray();
		$count = Career::count();
		if($count!=0){
			return view('frontend.careers',$this->data);
		}else{
			return view('frontend.no-vacancy',$this->data);			
		}
	}

	public function career_details($id) {
		$this->data['career'] = Career::where('id',$id)->get();
		return view('frontend.career-details',$this->data);
	}
	
	public function news() {
		$this->data['news'] = News::orderBy('updated_at',"DESC")->get();
		$this->data['count'] =$this->data['news']->count();
		return view('frontend.news',$this->data);
	}

	public function news_single($id) {
		$this->data['newss'] = News::orderBy('updated_at',"DESC")->where('id','<>',$id)->paginate(3);
		$this->data['news'] = News::where('id',$id)->get();
		$this->data['count'] =$this->data['news']->count();
		return view('frontend.news-single',$this->data);
	}

	public function product() {
		$this->data['products'] = Header::where('type','products')->get();
		$this->data['pepero'] = Product::where('sub_category_name','pepero')->get();
		$this->data['toppo'] = Product::where('sub_category_name','toppo')->get();
		$this->data['koala'] = Product::where('sub_category_name','koalas-march')->get();
		$this->data['custard_cake'] = Product::where('sub_category_name','custard-cake')->get();
		$this->data['choco_pie'] = Product::where('sub_category_name','choco-pie')->get();
		$this->data['xylitol'] = Product::where('sub_category_name','xylitol')->get();
		$this->data['fusen_no_mi'] = Product::where('sub_category_name','fusen-no-mi')->get();
		return view('frontend.product',$this->data);
	}
}