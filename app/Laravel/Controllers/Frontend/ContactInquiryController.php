<?php 

namespace App\Laravel\Controllers\Frontend;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\ContactInquiry;
/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\ContactInquiryRequest;

/**
*
* Additional classes needed by this controller
*/

use App\Laravel\Events\SendEmail;
use Event;
use Helper, Carbon, Session, Str, DB,File,Image,ImageUploader,Input;

class ContactInquiryController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
	}

	public function sent (ContactInquiryRequest $request) {
		try {
			$new_contact = new ContactInquiry;
			$new_contact->fill($request->all());
			if($request->hasFile('file')){
				$upload = $this->__upload($request);
				$new_contact->directory = $upload["directory"];
				$new_contact->filename = $upload["filename"];
			}

			if($new_contact->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"Your inquiry successfully sent.");
				$notification_data = new SendEmail(['input' => Input::all()]);
				Event::fire('send-email', $notification_data);
				return redirect()->route('frontend.contact');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	
}