<?php 

namespace App\Laravel\Controllers\Frontend;

use Illuminate\Support\Collection;
use App\Laravel\Controllers\Controller as MainController;
use Auth, Session;

class Controller extends MainController{

	protected $data;

	public function __construct(){
		self::set_backoffice_routes();
		self::set_user_info();
	}

	public function set_backoffice_routes(){
		/*$this->data['routes'] = array(
			"dashboard" => ['backoffice.dashboard'],
		);*/
	}

	public function set_user_info(){
		$this->data['auth'] = Auth::user();
	}
	
	public function get_data(){
		return $this->data;
	}
}