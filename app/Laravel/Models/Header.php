<?php

namespace App\Laravel\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;
use Carbon,Helper,Str;

class Header extends Authenticatable{
    use SoftDeletes,DateFormatterTrait;
    
    /**
     * Enable soft delete in table
     * @var boolean
     */
    protected $softDelete = true;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'header';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['filename','directory','type','path'];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    protected $appends = ['user_avatar'];


    public function getUserAvatarAttribute(){
        $avatar = "";

        if($this->filename){
            $avatar = "{$this->directory}/resized/{$this->filename}";
        }else{
            if($this->fb_id){
                $avatar = "https://graph.facebook.com/{$this->fb_id}/picture?type=large";
            }
        }

        return $avatar;
    }

    public function devices(){
        return $this->hasMany("App\Laravel\Models\UserDevice",'user_id','id');
    }

    public function shakes(){
        return $this->hasMany("App\Laravel\Models\UserShake",'user_id','shake_id');
    }

    public function logs(){
        return $this->hasMany("App\Laravel\Models\UserLog",'user_id','id')->orderBy('updated_at',"DESC");
    }

    public function interests(){
        return $this->hasMany("App\Laravel\Models\UserInterest",'user_id','id');
    }

}
