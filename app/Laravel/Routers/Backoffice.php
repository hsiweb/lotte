<?php

$router->group([

	/**
	*
	* Backend routes main config
	*/
	'namespace' => "Backoffice", 
	'as' => "backoffice.", 
	// 'domain' => env('BACKOFFICE_URL',"backoffice.pepero.localhost.com"), 
	'middleware'=> "web",
	'prefix'	=> "admin"

	], function(){

		
	/**
	*
	* Routes for guests
	*/
	$this->group(['middleware' => "backoffice.guest"], function(){
		$this->get('login',['as' => "login",'uses' => "AuthController@login"]);
		$this->post('login',['uses' => "AuthController@authenticate"]);
	});

	/**
	*
	* Routes for authenticated users
	*/
	$this->group(['middleware' => "backoffice.auth"], function(){
		
		$this->get('lock',['as' => "lock",'uses' => "AuthController@lock"]);
		$this->post('lock',['uses' => "AuthController@unlock"]);
		$this->get('logout',['as' => "logout",'uses' => "AuthController@destroy"]);

		$this->group(['middleware' => "backoffice.lock"], function(){
			$this->get('/',['as' => "dashboard",'uses' => "DashboardController@index"]);
		});

		$this->group(['prefix' => "header", 'as' => "header."], function(){
			$this->get('/',['as' => "index",'uses' => "HeaderController@index"]);
			$this->get('create',['as' => "create",'uses' => "HeaderController@create"]);
			$this->post('create',['uses' => "HeaderController@store"]);
			$this->get('edit/{id?}',['as' => "edit",'uses' => "HeaderController@edit"]);
			$this->post('edit/{id?}',['uses' => "HeaderController@update"]);
			$this->any('delete/{id?}',['as' => "destroy", 'uses' => "HeaderController@destroy"]);
			$this->get('deleted',['as' => "deletes",'uses' => "HeaderController@deleted"]);
		});

		$this->group(['prefix' => "image_slider", 'as' => "image_slider."], function(){
			$this->get('/',['as' => "index",'uses' => "ImageSliderController@index"]);
			$this->get('create',['as' => "create",'uses' => "ImageSliderController@create"]);
			$this->post('create',['uses' => "ImageSliderController@store"]);
			$this->get('edit/{id?}',['as' => "edit",'uses' => "ImageSliderController@edit"]);
			$this->post('edit/{id?}',['uses' => "ImageSliderController@update"]);
			$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ImageSliderController@destroy"]);
			$this->get('deleted',['as' => "deletes",'uses' => "ImageSliderController@deleted"]);
		});

		$this->group(['prefix' => "product", 'as' => "product."], function(){
			$this->get('/',['as' => "index",'uses' => "ProductController@index"]);
			$this->get('create/{category?}',['as' => "create",'uses' => "ProductController@create"]);
			$this->post('create/{category?}',['uses' => "ProductController@store"]);
			$this->get('edit/{id?}',['as' => "edit",'uses' => "ProductController@edit"]);
			$this->post('edit/{id?}',['uses' => "ProductController@update"]);
			$this->any('delete/{id?}',['as' => "destroy", 'uses' => "ProductController@destroy"]);
			$this->get('deleted',['as' => "deletes",'uses' => "ProductController@deleted"]);
		});

		$this->group(['prefix' => "news", 'as' => "news."], function(){
			$this->get('/',['as' => "index",'uses' => "NewsController@index"]);
			$this->get('create',['as' => "create",'uses' => "NewsController@create"]);
			$this->post('create',['uses' => "NewsController@store"]);
			$this->get('edit/{id?}',['as' => "edit",'uses' => "NewsController@edit"]);
			$this->post('edit/{id?}',['uses' => "NewsController@update"]);
			$this->any('delete/{id?}',['as' => "destroy", 'uses' => "NewsController@destroy"]);
			$this->get('deleted',['as' => "deletes",'uses' => "NewsController@deleted"]);
		});

		$this->group(['prefix' => "careers", 'as' => "career."], function(){
			$this->get('/',['as' => "index",'uses' => "CareerController@index"]);
			$this->get('create',['as' => "create",'uses' => "CareerController@create"]);
			$this->post('create',['uses' => "CareerController@store"]);
			$this->get('edit/{id?}',['as' => "edit",'uses' => "CareerController@edit"]);
			$this->post('edit/{id?}',['uses' => "CareerController@update"]);
			$this->any('delete/{id?}',['as' => "destroy", 'uses' => "CareerController@destroy"]);
			$this->get('deleted',['as' => "deletes",'uses' => "CareerController@deleted"]);
		});

		$this->group(['prefix' => "taglines", 'as' => "tagline."], function(){
			$this->get('/',['as' => "index",'uses' => "TaglineController@index"]);
			$this->get('create',['as' => "create",'uses' => "TaglineController@create"]);
			$this->post('create',['uses' => "TaglineController@store"]);
			$this->get('edit/{id?}',['as' => "edit",'uses' => "TaglineController@edit"]);
			$this->post('edit/{id?}',['uses' => "TaglineController@update"]);
			$this->any('delete/{id?}',['as' => "destroy", 'uses' => "TaglineController@destroy"]);
			$this->get('deleted',['as' => "deletes",'uses' => "TaglineController@deleted"]);
		});

		$this->group(['prefix' => "contact", 'as' => "contact."], function(){
			$this->get('/',['as' => "index",'uses' => "ContactInquiryController@index"]);
		});
	});
});
