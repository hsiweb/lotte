<?php

$router->group([

	/**
	*
	* Backend routes main config
	*/
	'namespace' => "Frontend", 
	'as' => "frontend.", 
	// 'domain' => env('BACKOFFICE_URL',"backoffice.pepero.localhost.com"), 
	'middleware'=> "web"

	], function(){
	
	$this->get('/',['as'=>"index",'uses' => "HomeController@index"]);
	$this->get('about',['as'=>"about",'uses' => "HomeController@about"]);
	$this->get('product',['as'=>"product",'uses' => "HomeController@product"]);
	$this->get('news',['as'=>"news",'uses' => "HomeController@news"]);
	$this->get('contact',['as'=>"contact",'uses' => "HomeController@contact"]);
	$this->get('news/{id?}',['as'=>"news-single",'uses' => "HomeController@news_single"]);
	$this->get('careers',['as'=>"careers",'uses' => "HomeController@careers"]);
	$this->get('careers/{id?}',['as'=>"career_details",'uses' => "HomeController@career_details"]);
	$this->post('contact',['as'=>"contact",'uses' => "ContactInquiryController@sent"]);
});