<?php

namespace App\Laravel\Middleware\Api;

use Closure;
use Illuminate\Contracts\Auth\Guard;

use App\Laravel\Models\Article;
use Input;

class ValidArticle {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		// $this->cache_expiration = Helper::get_cache_expiry();
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

		$article_id = Input::get('article_id',0);
		$article = Article::select("id")->where('id',$article_id)->where('status',"published")->first();

		if(!$article){
			$response = array(
					"msg" => "Record not found.",
					"status" => FALSE,
					'status_code' => "RECORD_NOT_FOUND"
				);
			$response_code = 404;

			return response($response, $response_code);
		}

		return $next($request);
	}

}
