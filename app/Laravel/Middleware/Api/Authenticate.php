<?php

namespace App\Laravel\Middleware\Api;

use Closure;
use Illuminate\Contracts\Auth\Guard;

use App\Laravel\Models\User;
use Input,Carbon,Cache,Helper;

class Authenticate {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		// $this->cache_expiration = Helper::get_cache_expiry();
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

		$user_id = Input::get('auth_id',0);

		// $user =  Cache::remember("user:user_id={$user_id}",$this->cache_expiration,function()use($user_id){
		// 	return User::find($user_id);
		// });

		$user = User::find($user_id);

		if(!$user OR ($user AND (!in_array($user->type, ['user'])))){
			$response = array(
					"msg" => "Invalid User.",
					"status" => FALSE,
					'status_code' => "INVALID_USER"
				);
			$response_code = 401;

			return response($response, $response_code);
		}

		return $next($request);
	}

}
