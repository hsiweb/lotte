<?php

namespace App\Laravel\Services;

use Route, Str, Carbon, Input, DB, DateTime, DateInterval, DatePeriod;

use App\Laravel\Models\CitizenRequest;
use App\Laravel\Models\MacRequest;
use App\Laravel\Models\Queue;

use App\Laravel\Models\CRModule;
use App\Laravel\Models\CRCategory;
use App\Laravel\Models\CRSubCategory;
use App\Laravel\Models\CRProcess;
use App\Laravel\Models\CRTracker;
use App\Laravel\Models\CRTrackerHeader;

class Helper{

  	/**
	 * Parse date to the specified format
	 *
	 * @param date $time
	 * @param string $format
	 *
	 * @return Date
	 */
	public static function date_format($time,$format = "M d, Y @ h:i a") {
		return $time == "0000-00-00 00:00:00" ? "" : date($format,strtotime($time));
	}

	/**
	 * Parse date to the 'date only' format
	 *
	 * @param date $time
	 * @param string $format
	 *
	 * @return Date
	 */
	public static function date_only($time) {
		return Self::date_format($time, "F d, Y");
	}

	/**
	 * Parse date to the 'time only' format
	 *
	 * @param date $time
	 * @param string $format
	 *
	 * @return Date
	 */
	public static function time_only($time) {
		return Self::date_format($time, "g:i A");
	}

	/**
	 * Parse date to the standard sql date format
	 *
	 * @param date $time
	 * @param string $format
	 *
	 * @return Date
	 */
	public static function date_db($time) {
		return $time == "0000-00-00 00:00:00" ? "" : date(env('DATE_DB',"Y-m-d"),strtotime($time));
	}

	/**
	 * Parse date to the standard sql datetime format
	 *
	 * @param date $time
	 * @param string $format
	 *
	 * @return date
	 */
	public static function datetime_db($time) {
		return $time == "0000-00-00 00:00:00" ? "" : date(env('DATETIME_DB',"Y-m-d H:i:s"),strtotime($time));
	}

	/**
	 * Parse date to a greeting
	 *
	 * @param date $time
	 *
	 * @return string
	 */
	public static function greet($time = NULL) {
		if(!$time) $time = Carbon::now();
		$hour = date("G",strtotime($time));

		if($hour < 5) {
			$greeting = "You woke up early";
		}elseif($hour < 10){
			$greeting = "Good morning";
		}elseif($hour <= 12){
			$greeting = "It's almost lunch";
		}elseif($hour < 18){
			$greeting = "Good afternoon";
		}elseif($hour <= 22){
			$greeting = "Good evening";
		}else{
			$greeting = "You must be working really hard";
		}

		return $greeting;
	}

	/**
	 * Get all months within a range
	 *
	 * @param date $time
	 *
	 * @return string
	 */
	public static function months_within_range($start, $end, $format = "F") {
		$start    = (new DateTime($start))->modify('first day of this month');
		$end      = (new DateTime($end))->modify('first day of next month');
		$interval = DateInterval::createFromDateString('1 month');
		$period   = new DatePeriod($start, $interval, $end);

		$months = [];
		foreach ($period as $dt) {
		    array_push($months, $dt->format($format));
		}
		return $months;
	}

	/**
	 * Shows time passed
	 *
	 * @param date $time
	 *
	 * @return string
	 */
	public function time_passed($time){
		$current_date = Carbon::now();
		$secsago = strtotime($current_date) - strtotime($time);
		$nice_date = 1;
		if ($secsago < 60):
			if($secsago < 30){ return "Just Now.";}
		    $period = $secsago == 1 ? '1 second'     : $secsago . ' seconds';
		elseif ($secsago < 3600) :
		    $period    =   floor($secsago/60);
		    $period    =   $period == 1 ? '1 minute' : $period . ' minutes';
		elseif ($secsago < 86400):
		    $period    =   floor($secsago/3600);
		    $period    =   $period == 1 ? '1 hour'   : $period . ' hours';
		elseif ($secsago < 432000):
		    $period    =   floor($secsago/86400);
		    $period    =   $period == 1 ? '1 day'    : $period . ' days';
		else:
		   $nice_date = 0;
		   $period = date("M d, Y",strtotime($time));
		endif;
		if($nice_date == 1)
			return $period." ago";
		else
			return $period;
	}

	public static function mins_to_time($mins) {
		$seconds = $mins * 60;
	    $dtF = new \DateTime('@0');
	    $dtT = new \DateTime("@$seconds");
	    return $dtF->diff($dtT)->format('%aD %hH %iM');
	}

	/**
	 * Checks if route is active
	 *
	 * @param array $routes
	 * @param string $class
	 *
	 * @return string
	 */
	public static function is_active(array $routes, $class = "active") {
		return  in_array(Route::currentRouteName(), $routes) ? $class : NULL;
	}

	/**
	 * Generate seven uppercase characters randomly
	 *
	 * @return string
	 */
	public static function create_ucode(){
		return Str::upper(Str::random(7));
	}

	/**
	 * Create a filename
	 *
	 * @param string $extension
	 *
	 * @return string
	 */
	public static function create_filename($extension){
		return Str::lower(Str::random(10).date("mdYhs")).".".$extension;
	}

	/**
	 * Gets the excerpt of a string
	 *
	 * @param string $str
	 *
	 * @return string
	 */
	public static function get_excerpt($str){
		$paragraphs = explode("<br>", $str);
	    return Str::words(strip_tags($paragraphs[0]),20);
	}

	/**
	 * Improved array diff method
	 *
	 * @param array $a
	 * @param array $b
	 *
	 * @return array
	 */
	public static function array_diff($a, $b) {
	    $map = $out = array();
	    foreach($a as $val) $map[$val] = 1;
	    foreach($b as $val) if(isset($map[$val])) $map[$val] = 0;
	    foreach($map as $val => $ok) if($ok) $out[] = $val;
	    return $out;
	}

	/**
	 * Gets the slug of a string
	 *
	 * @param string $str
	 * @param string $tbl
	 * @param string $col
	 *
	 * @return string
	 */
	public static function get_slug($tbl, $col, $val){
		$slug = Str::slug($val);
		$check_slug = DB::table($tbl)->where("{$col}",'like',"%{$val}%")->count();
		if($check_slug != 0) $slug .= ++$check_slug;
		return $slug;
	}

	/**
	 * Translates a number to a short alhanumeric version
	 *
	 * Translated any number up to 9007199254740992
	 * to a shorter version in letters e.g.:
	 * 9007199254740989 --> PpQXn7COf
	 *
	 * specifiying the second argument true, it will
	 * translate back e.g.:
	 * PpQXn7COf --> 9007199254740989
	 *
	 * @param mixed   $in	  String or long input to translate
	 * @param boolean $to_num  Reverses translation when true
	 * @param mixed   $pad_up  Number or boolean padds the result up to a specified length
	 * @param string  $pass_key Supplying a password makes it harder to calculate the original ID
	 *
	 * @return mixed string or long
	 */
	public static function alphaID($in, $to_num = false, $pad_up = false, $pass_key = null) {
		$out   =   '';
		$index = 'abcdefghijklmnopqrstuvwxyz0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$base  = strlen($index);

		if ($pass_key !== null) {
			// Although this function's purpose is to just make the
			// ID short - and not so much secure,
			// with this patch by Simon Franz (http://blog.snaky.org/)
			// you can optionally supply a password to make it harder
			// to calculate the corresponding numeric ID

			for ($n = 0; $n < strlen($index); $n++) {
				$i[] = substr($index, $n, 1);
			}

			$pass_hash = hash('sha256',$pass_key);
			$pass_hash = (strlen($pass_hash) < strlen($index) ? hash('sha512', $pass_key) : $pass_hash);

			for ($n = 0; $n < strlen($index); $n++) {
				$p[] =  substr($pass_hash, $n, 1);
			}

			array_multisort($p, SORT_DESC, $i);
			$index = implode($i);
		}

		if ($to_num) {
			$len = strlen($in) - 1;
			for ($t = $len; $t >= 0; $t--) {
				$bcp = bcpow($base, $len - $t);
				$out = $out + strpos($index, substr($in, $t, 1)) * $bcp;
			}
			if (is_numeric($pad_up)) {
				$pad_up--;
				if ($pad_up > 0) {
					$out -= pow($base, $pad_up);
				}
			}
		} else {
			// Digital number  -->>  alphabet letter code
			if (is_numeric($pad_up)) {
				$pad_up--;
				if ($pad_up > 0) {
					$in += pow($base, $pad_up);
				}
			}
			for ($t = ($in != 0 ? floor(log($in, $base)) : 0); $t >= 0; $t--) {
				$bcp = bcpow($base, $t);
				$a   = floor($in / $bcp) % $base;
				$out = $out . substr($index, $a, 1);
				$in  = $in - ($a * $bcp);
			}
		}
		return $out;
	}

	/**
	* Remove special chars from a string
	*
	* @var string $str
	*
	* @return int
	*/
	public static function str_clean($str){
	   $str = str_replace(' ', '-', $str); // Replaces all spaces with hyphens.
	   $str = preg_replace('/[^A-Za-z0-9\-]/', '', $str); // Removes special chars.
	   return preg_replace('/-+/', '-', $str); // Replaces multiple hyphens with single one.
	}

	public static function progress_color($percentage){
		if($percentage > 75){
			return "bg-success";
		}elseif($percentage > 50){
			return "bg-warning";
		}else{
			return "bg-danger";
		}
	}

	public static function validation_token(){
		return Str::lower(date("y").Str::random(4).date("m"));
	}

	public static function account_type($type){
		$type = Str::lower($type);
		switch ($type) {
			case 'super_user':
				$type = "Super User";
			break;
			case 'employee':
				$type = "Assessor Employee";
			break;
			case 'admin':
				$type = "Assessor Administrator";
			break;

			default:
				$type = Str::title(str_replace("_", " ", $type));
			break;
		}

		return $type;
	}

	public static function service_code(array $data = ['type' => NULL , 'appointment_date' => NULL]){
		$type = Str::lower($data['type']);

		switch($type){
			case 'hospitals':
			case 'regional_dswd':
			case 'local_registry':
			case 'schools':
				$date = self::date_db($data['appointment_date']);

				// dd($date);
				$service = "social";

				$social_request = CitizenRequest::select("id")->where('sub_type',$service)
										->lists("id")->toArray();

				if(count($social_request) == 0){

					$num_of_queue = 0;
					goto s1;
				}
				$num_of_queue = MacRequest::whereIn('citizen_request_id',$social_request)
										->whereRaw("DATE(schedule_appointment) = '{$date}'")->count();
				s1:
				return "SOCIAL-".str_pad(++$num_of_queue, 4, 0, STR_PAD_LEFT);
			break;
			case 'tesda':
				$date = self::date_db($data['appointment_date']);

				// dd($date);
				$service = "scholar";

				$social_request = CitizenRequest::select("id")->where('sub_type',$service)
										->lists("id")->toArray();

				if(count($social_request) == 0){

					$num_of_queue = 0;
					goto s2;
				}
				$num_of_queue = MacRequest::whereIn('citizen_request_id',$social_request)
										->whereRaw("DATE(schedule_appointment) = '{$date}'")->count();

				s2:
				return "SCHOLAR-".str_pad(++$num_of_queue, 4, 0, STR_PAD_LEFT);
			break;
		}
	}

	public static function get_service($type){
		$type = Str::lower($type);

		$result = "";
		switch($type){
			case 'hospitals':
			case 'regional_dswd':
			case 'local_registry':
			case 'schools':
				$result = "social";
			break;
			case 'tesda':
				$result = "scholar";
			break;
		}
		return $result;
	}

	public static function get_process_title($code){

		$result = "";
		switch(Str::lower($code)){
			case 'info_validation':
				$result = "Validation of Information";
			break;
			case 'assessment_evaluation':
				$result = "Assessment Evaluation";
			break;
			case 'final_interview':
				$result = "Final Interview";
			break;
			case 'requirement_submission':
				$result = "Submission of Requirements";
			break;
		}

		return $result;
	}

	public static function get_service_title($type){
		$type = Str::lower($type);

		$result = "";
		switch($type){
			case 'hospitals':
				$result = "Public and Private Hospitals";
			break;
			case 'regional_dswd':
				$result = "Regional Office DSWD";
			break;
			case 'local_registry':
				$result = "Local Civil Registry";
			break;
			case 'schools':
				$result = "Public and Private Schools and Universities";
			break;
			case 'tesda':
				$result = "TESDA";
			break;
		}
		return $result;
	}

	public static function next_sequence($service_type) {

		$service_type = Str::lower($service_type);

		$current_sequence = Queue::select('sequence_no')
							->where('service_type', $service_type)
							->whereDate("created_at", "=", Carbon::today()->toDateString())
							->orderBy('sequence_no',"DESC")
							->withTrashed()
							->first();

		return str_pad( ($current_sequence ? ($current_sequence->sequence_no + 1) : 1), 4, 0, STR_PAD_LEFT);

	}

	public static function create_queue_no($service_type, $sequence_no, $source ="webapp") {
		$service_type = Str::lower($service_type);
		$code = ($source == "mobile" ? "M" : "");

		switch ($service_type) {
			case 'priority_lane'		: 	$code .= "P"; break;
			case 'health_service'		: 	$code .= "H"; break;
			case 'scholarship'			: 	$code .= "SCHOLAR"; break;
			case 'scholarship_program'	: 	$code .= "E"; break;
			case 'social_service'		: 	$code .= "S"; break;
			case 'legal_service'		: 	$code .= "L"; break;
			case 'administrative'		: 	$code .= "A"; break;
		}

		return "{$code}-{$sequence_no}";

	}

	public static function create_request_code($category_id, $appointment_schedule) {
		$code = "";
		$category = CRCategory::find($category_id);

		$service_type = $category ? Str::lower($category->code) : NULL;
		switch ($service_type) {
			// case 'priority_lane'		: 	$code .= "PRIORITY"; break;
			case 'health_service'		: 	$code .= "HEALTH"; break;
			case 'scholarship'			: 	$code .= "SCHOLAR"; break;
			case 'scholarship_program'	: 	$code .= "SCHOLAR"; break;
			case 'social_service'		: 	$code .= "SOCIAL"; break;
			case 'legal_service'		: 	$code .= "LEGAL"; break;
			case 'administrative'		: 	$code .= "ADMIN"; break;
		}

		$current_sequence = CRTrackerHeader::where('category_code', ($category ? $category->code : ""))
							->whereDate("created_at", "=", self::date_db($appointment_schedule))
							->count();

		$next_sequence = str_pad( $current_sequence + 1, 4, 0, STR_PAD_LEFT);
		return "{$code}-{$next_sequence}";

	}

	public static function get_service_display($code , $return_key = "title"){
		$result = "";
		$title = "";
		$_class = "";
		$_icon = "";
		$_icon_color = "";


		switch($code){
			case 'health_service':
				$title = "Health Service";
				// _class =
				$_icon = "icon-pulse2";
				$_icon_color = "border-purple text-purple-300";
			break;
			case 'scholarship':
			case 'scholarship_program':
				$title = "Scholarship Program";
				$_icon = "icon-books";
				$_icon_color = "border-warning text-warning-300";
			break;
			case 'social_service':
				$title = "Social Service";
				$_icon = "icon-accessibility2";
				$_icon_color = "border-pink text-pink-300";
			break;
			case 'legal_service':
				$title = "Legal Service";
				$_icon = "icon-balance";
				$_icon_color = "border-brown text-brown-300";
			break;
			case 'administrative':
				$title = "Administrative";
				$_icon = "icon-people";
				$_icon_color = "border-slate text-slate-300";
			break;

		}

		switch($return_key){
			case 'icon':
				$result = $_icon;
			break;

			case 'icon_color':
				$result = $_icon_color;
			break;

			case 'class':
				$result = $_class;
			break;
			default :

			$result = $title;
		}

		return $result;
	}

  public static function syllabus($string){
    $split = str_split(str_replace("-","",$string));

    return implode(",",$split);

  }

}
