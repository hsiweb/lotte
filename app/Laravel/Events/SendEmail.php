<?php namespace App\Laravel\Events;

use Illuminate\Queue\SerializesModels;
use Mail,Str;
// use App\Constech\Models\GeneralSetting;

class SendEmail extends Event {

	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct(array $form_data)
	{
		$this->input = $form_data['input'];
	}

	public function job(){
		$data = ['name' => $this->input['name'],'subject' => $this->input['subject'],
				 'email' => $this->input['email'],'msg' => $this->input['message'],
				 'contact_no' => $this->input['contact_no']
				];

		Mail::send('emails.contact', $data, function($message){
			$message->from('support@lotte.com.ph');
			$message->to("info@lottepilipinas.com",env('APP_TITLE')." Support Team");
			$message->cc("joshuaarosco.hs@gmail.com");
			$message->subject("Contact Inquiry");
		   
		});
	}

}
