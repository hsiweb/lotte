<?php 

namespace App\Laravel\Events;

use Illuminate\Queue\SerializesModels;
use App\Laravel\Models\UserDevice;
use App\Laravel\Models\UserLog;

// use LaravelFCM\Message\OptionsBuilder;
// use LaravelFCM\Message\PayloadDataBuilder;
// use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

use App\Laravel\Services\FCM as Firebase;

class FCMNotification extends Event {

	use SerializesModels;

	protected $data;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct(array $form_data)
	{
		$this->data = $form_data;
	}

	public function job(){

		$log = new UserLog;
		$log->fill($this->data);
		$log->user_id = $this->data['auth_id'];
		$log->msg = $this->data['msg'];
		$log->save();

		$arr = array();
		$devices = UserDevice::where('user_id',$this->data['auth_id'])->where('is_login','1')->get();
		foreach ($devices as $key => $device) {
			// $optionBuiler = new OptionsBuilder();
			// $optionBuiler->setTimeToLive(60*20);

			// $notificationBuilder = new PayloadNotificationBuilder('Shake Now!');
			// $notificationBuilder->setBody('Shake to win amazing prizes.')
			//                     ->setSound('default');

			// $dataBuilder = new PayloadDataBuilder();
			// $dataBuilder->addData(['type' => 'SHAKE_BLAST']);

			// $option = $optionBuiler->build();
			// $notification = $notificationBuilder->build();
			// $data = $dataBuilder->build();

			// $token = $device->reg_id;

			// $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

			$fcm = new Firebase;
			$fcm_response = $fcm->send($device->reg_id,[
				"data" => [
					'type' => $this->data['type'],
					'sub_type' => $this->data['sub_type'],
					'reference_id' => $this->data['reference_id'],
					'title' => $this->data['title'],
					'msg' => $this->data['msg'],
					'thumbnail' => $this->data['thumbnail'],
				],
				'notification' => [
					'title' => "YOUR_TITLE_HERE",
					'body' => "YOUR_MSG_HERE",
				],
				'priority' => "high",
			]);

			array_push($arr, $fcm_response);
		}

		// dd($arr);
	}



}
