<?php namespace App\Laravel\Events;

use Illuminate\Queue\SerializesModels;
use App\Laravel\Models\Masterfile\User;
use Mail,Str,Helper,Cache;

class EmailNotification extends Event {

	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct(array $form_data)
	{
		$this->user_id = $form_data['user_id'];
		$this->msg = $form_data['msg'];
		// $this->cache_expiration = Helper::get_cache_expiry();
	}

	public function job(){
		$user_id = $this->user_id;

		// $user = Cache::remember("user:user_id={$user_id}",$this->cache_expiration,function()use($user_id){
			return User::find($user_id);
		// });

		$data = ["msg" => $this->msg, "user_id" => $user->id];

		Mail::send('emails.notification', $data, function($message) use ($user){
			$message->from("no-reply@krops.ph");
			$message->to($user->email, $user->name);
		    $message->subject("Krops.ph - Email Notification");
		});
	}

}
